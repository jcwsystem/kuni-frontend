$(function() {
	var logwork = function(xml) {

		var $xml =$(xml).find("logwork");
		var seminarid = $xml.find("seminarId").text();
		var cid = $xml.find("cId").text();
		var liveid = $xml.find("liveId").text();
		// URLからplayerid を生成
		var hierarchy = location.pathname.split('/');
		var uindex = hierarchy.length
		var bandwidth = hierarchy[uindex - 2];
		if (hierarchy[uindex - 4].indexOf("_vod")  !== -1 ) {
			var playerid = "vod" ; 
		} else {
			var playerid = hierarchy[uindex - 3] ; 
		}
		playerid = playerid + bandwidth.slice(0, bandwidth.length - 1);
		
		var lw_mliveid = getCookie("liveid");
		var lw_userid = getCookie("uid");
		console.log( cid, liveid, seminarid, lw_mliveid, lw_userid, playerid);
		//logworks.add({cid: "91f9hnju", liveid: "live_medpeer", seminarid: seminarid, mliveid: lw_mliveid, userid: lw_userid, playerid: playerid});
		logworks.add({cid: cid, liveid: liveid, seminarid: seminarid, mliveid: lw_mliveid, userid: lw_userid, playerid: playerid});	
	}

	$.ajax({
		type: "GET",
		url: xmlPath + "common/xml/setting.xml",
		dataType: "xml",
		success: logwork
	});
});
