/*
* 時間設定xmlファイルを所定間隔(60sec)で読込み，リンクボタンの活性／不活性を切り替える
* rev.0.2	SEP/3/2020	HTML要素の読込を確実に行うためにgetTime()をcallback関数とした
*/
(function() {
	var server_sts = 0;
	// オブジェクト定義
	var titleStatusText = {
		live:		"［Live］"
	}
	var titleStatus = [
		titleStatusText.live,		// 0
		titleStatusText.live,		// 1
		titleStatusText.live		// 2
	];
	var watchButton = [
		// 0
		{
			live:		'<div id="enquete_off"><p>視聴後アンケートへ回答する</p></div>'
		},
		// 1
		{
			live:		'<div id="enquete"><a href="#linkUrl" target="_blank"><p>視聴後アンケートへ回答する</p></a></div>'
		},
		// 2
		{
			live:		'<div id="enquete_off"><p>視聴後アンケートへ回答する</p></div>'
		}
	];

	// index.htmlのライブボタンのid要素
	var watchButtonId = {
		live: 'enquete'
	};

	var statusNo = {
		live: {
			before: 0,
			open: 1,
			close: 2
		}
	};
	// 設定パラメータxmlファイル名
	var xmlFile = "common/xml/timeTable.xml";
	var xmlData = "";
	// ボタンのリンク先URL
	var linkUrl = "";

	// 現在時刻
	var now = "";

	//	timeTableの設定時刻に応じてアンケートボタンの活性／非活性を切り替える
	var refreshBtn = function(xml) {
		xmlData = xml;																		// xmlファイルの中身
		// getUrl(getTime);																	// getUrl()　⇒　getTime()の順に実行
		getUrl(function(){
			showLink(now, 'live');													// getUrl　⇒　showLinkの順に実行
		});
	}

	// readXml()実行時にhiddenタグ要素のパラメータを取得できるまで繰り返す関数
	// <input type="hidden" name="linkurl" value="xxxxxx">タグからリンク先URL取得
	var getUrl = function(callback) {
		var lCnt = 0;
		var surl = "";
		var timer = setInterval(function() {
			lCnt++;
			// リトライは５回まで，リンク先URLがundefind の場合はここで終わり;
			if (lCnt > 5) {
				clearInterval(timer );
				return;
			} else {
				surl = $("input[name='linkurl']").val();
				if (surl.length > 0) {
					linkUrl = surl;
					clearInterval(timer );
					callback();
					return;
				}
			}
		}, 1000);																						// １秒毎に繰り返し
	}
	/*
	* [時刻設定].xmlから開始・終了時刻を取得し，htmlを動的に書換える関数
	*/
		// [時刻設定].xmlの要素(type)の子要素を時刻(now)と比較してライブボタンタグを生成する
		var showLink = function(now, type) {
			var status = parseInt($(xmlData).find("schedule").attr("status"), 10);	// 直接表示ステータス値
			var $type = $(xmlData).find("schedule " + type);															 // scheduleかつ[type]の子要素全体
//		var url   = $type.find("link").attr("url");																						// リンク先URL --> グルーバル変数:linkUrlに変更

			if (isNaN(status)) {  																									// 直接表示ステータスが未設定の時
				// オープンクローズ時間[配列定義]
				var open = [];
				var close = [];
				$type.find("session").each(function() {
				if($(this).find("opentime").text() !== "") {
					open.push($(this).find("opentime").text());
					close.push($(this).find("closetime").text());
				}
			});

			// open配列の設定値によるループ
			var statusTxt = "";

			// 現在時刻をunixtime形式に変換
			now = now.getTime();

			for (var index = 0; index <= open.length - 1 ; index++) {
				var chkFlgOpen  = chkTimeFormat(open[index]);
				var chkFlgClose = chkTimeFormat(close[index]);
				var from = new Date(open[index]).getTime();
				var to = new Date(close[index]).getTime();

				// 無効な時刻設定
				if ((chkFlgOpen) ||	(chkFlgClose) || (from >= to) ) {
					statusTxt = "before";
					break;
				}
				// 設定値によってボタンの状態を変える
				if (now < from) {
					statusTxt = "before";
					break;
				} else if( (from < now) && (now < to) ) {
					statusTxt = "open";
					break;
				}	else if (to < now) {
					statusTxt = "close";
					if (index === open.length - 1) {										// 最後の時間設定値の場合
						break;
					}
				} else {
					statusTxt = "before";
					break;
				}
			}
			createLink(statusNo[type][statusTxt], type, linkUrl);				// ボタンオブジェクトを生成
		// statusを直接指定した場合
		} else {
			createLink(status, type, linkUrl);
			return;
		}
	}

	// ボタンのリンク先URLを設定する
	var createLink = function(status, type, url) {
		if (watchButton[status]) {
			$("#" + watchButtonId[type]).html(watchButton[status][type].replace("#linkUrl", url));
			$(".seriesName").html(
				titleStatus[status]
				+ $(xmlData).find("titleSection").find("seriesName").text()
			);
		} else {
			$("#" + watchButtonId[type]).html("");									// watchButton[status]が undefind の場合
		}
	}

	// 時刻書式になっているか判定
	var chkTimeFormat = function(str) {
		if(!str.match(/^\d{4}\/\d{2}\/\d{2}\s\d{2}\:\d{2}\:\d{2}$/)){
			return true;
		}
		return false;
	}

	// 時刻設定xmlファイルを読込むcallback関数
	var readXml = function() {
		$.ajax({
			type: "GET",
			cache: false,　　　　　　   	// 受信結果をキャッシュしない
			url: xmlPath + xmlFile,				 // 参照ファイル
			dataType: "xml",							 // 応答データの種類
			timeout: 10000,	// タイムアウト10秒
//			success: getTime							 // 読込成功時に呼出す関数
			// success: refreshBtn
		}).done(function(data, status, xhr){
			// レスポンスヘッダーで時刻取得
			var responseHeader = xhr.getResponseHeader('Date');
			if (responseHeader != null) {
				now = new Date(responseHeader);
			} else {
				// レスポンスヘッダーで時刻が取れなかった場合、クライアントタイムで取得
				now = new Date();
			}
			// 表示処理実行
			refreshBtn(data);
		});
	}

	// インターバル60秒で繰り返しreadXml()を実行
	var init = function() {
		$(function() {
			readXml();												// 読込直後の実行
			testTimer=setInterval(function(){
					readXml();
			} , 60000);
		});
	}
	var testTimer = "";

	init();
})();
