$(function() {
	var main = function(xml) {
		var $xml;

		var $stream = $(xml).find("stream");

		var $header = $(xml).find("header");
		var $copyright = $(xml).find("copyright");

		var $style = $(xml).find("style");

		var $live = $(xml).find("live");
		var $speaker = $(xml).find("speaker");
		var $ondemand = $(xml).find("ondemand");
		var $watchTest = $(xml).find("watchTest");
		var $enquete = $(xml).find("enquete");
		var $product1 = $(xml).find("product1");
		var $required = $(xml).find("required");
		var $support = $(xml).find("support");

		var $reaction = $(xml).find("reaction");
		var $pageSwitch = $(xml).find("pageSwitch")
		var $attachment = $(xml).find("attachment")
		var $questions = $(xml).find("questions")
		var $bbsbox = $(xml).find("bbsbox")

		/* --------------------------------------------------
			ストリーム書き出し
		-------------------------------------------------- */
		$("#stream300k").attr("src", $stream.find("stream300k").text());
		$("#stream800k").attr("src", $stream.find("stream800k").text());
		$("#dvr300k").attr("src", $stream.find("dvr300k").text());
		$("#dvr800k").attr("src", $stream.find("dvr800k").text());


		/* --------------------------------------------------
			headerとcopyright書き出し
		-------------------------------------------------- */
		$xml = $header;
		var header = {
			color: 		$xml.attr("color"),
			bgColor: 	$xml.attr("bgColor"),

			title: 			$xml.find("title").text(),
			titleSize: 		$xml.find("title").attr("size"),
			subject: 		$xml.find("subject").text(),
			subjectSize:	$xml.find("subject").attr("size"),

			description: 		$xml.find("description").text(),
			descriptionSize:	$xml.find("description").attr("size"),
			descriptionColor:	$xml.find("description").attr("color"),
			descriptionBgColor:	$xml.find("description").attr("bgColor"),

			sponsorship: 	$xml.find("sponsorship").text()
		}

		$("#pageTitle").css({
			"color": 			header.color,
			"background-color":	header.bgColor
		});

		$("#pageTitle p").html(header.title).css("font-size", header.titleSize);
		$("#pageTitle h1").html(header.subject).css("font-size", header.subjectSize);
		$("#description").html(header.description).css({
			"font-size": 		header.descriptionSize,
			"color": 			header.descriptionColor,
			"background-color":	header.descriptionBgColor
		});
		$("#header p.sponsorship").html(header.sponsorship);

		$("#copyright").html($copyright.text());


		/* --------------------------------------------------
			書き出し
		-------------------------------------------------- */
		$xml = $live;
		var live = createObj($xml);
		live.schedule = $xml.find("schedule").text();

		$xml = $speaker;
		var speaker = createObj($xml);

		$xml = $ondemand;
		var ondemand = createObj($xml);
		ondemand.scheduleStart =	$xml.find("schedule start").text();
		ondemand.scheduleEnd =		$xml.find("schedule end").text();

		var watchTest = createObj($watchTest);
		var required = createObj($required);

		$xml = $support;
		var support = $xml.text();

		writeCommonElement("#live", live);
		writeCommonElement("#speaker", speaker);
		writeCommonElement("#ondemand", ondemand);
		writeCommonElement("#watchTest", watchTest);
		writeCommonElement("#required", required);

		$("#liveSchedule").html(live.schedule);
		$("#ondemandSchedule h3").eq(0).html(ondemand.scheduleStart);
		$("#ondemandSchedule h3").eq(1).html(ondemand.scheduleEnd);
		$("#support").html(support);

		$speaker.find("speakerList").each(function() {
			$("#speaker .main").append(
				$("<p>").html($(this).text())
			);
		});

		// リアルタイムアンケート・Goodボタン
		var realtime = {
			realtimeQ:		$reaction.find("realtimeQ").text(),
			goodBtn:			$reaction.find("goodBtn").text()
		}
		$("#realtimeQ").html(realtime.realtimeQ);
		$("#goodBtn").html(realtime.goodBtn);

		// BBS質問ボックス
		$("#bbsBox").html($bbsbox.text());

		// 視聴ページ切替ボタン
		var switching = {
			currentLow:		$pageSwitch.find("currentLow").text(),
			currentHigh:	$pageSwitch.find("currentHigh").text(),
			liveLow:			$pageSwitch.find("liveLow").text(),
			liveHigh:			$pageSwitch.find("liveHigh").text(),
			dvrLow:				$pageSwitch.find("dvrLow").text(),
			dvrHigh:			$pageSwitch.find("dvrHigh").text()
		}
		$("#currentLow").html(switching.currentLow);
		$("#currentHigh").html(switching.currentHigh);
		$("#liveLow").html(switching.liveLow);
		$("#liveHigh").html(switching.liveHigh);
		$("#dvrLow").html(switching.dvrLow);
		$("#dvrHigh").html(switching.dvrHigh);

		// 添付文書
		var attach = {
			nbiBtn:			$attachment.find("nbiBtn"),
			lillyDoc:		$attachment.find("lillyDoc"),
			daichDoc:		$attachment.find("daichDoc").text()
		}
		// NBIボタン（リンク先がブランクの時非表示）
		$(attach.nbiBtn).find("a").attr("href").length && $("#nbiBtn").append(
			$("<p>").attr({
				"style":		$(attach.nbiBtn).find("p").attr("style")
			}).append(
			$("<a>").attr({
				"href": 		$(attach.nbiBtn).find("a").attr("href"),
				"target":	 $(attach.nbiBtn).find("a").attr("target"),
				"class":	 $(attach.nbiBtn).find("a").attr("class")
			})
			.html($(attach.nbiBtn).find("a").text())
		));
		// Lilly_添付文書（リンク先がブランクの時非表示）
		$(attach.lillyDoc).find("a").attr("href").length && $("#lillyDoc").append(
			$("<p>").attr({
				"style":		$(attach.lillyDoc).find("p").attr("style")
			}).append(
			$("<a>").attr({
				"href": 	$(attach.lillyDoc).find("a").attr("href"),
				"target":	 $(attach.lillyDoc).find("a").attr("target"),
				"class":	 $(attach.lillyDoc).find("a").attr("class")
			})
			.html($(attach.lillyDoc).find("a").text())
		));
		// 第一三共_添付文書（VOD）
		$("#daichDoc").html(attach.daichDoc);

		// 質問フォーム(../../form/index.html)への設定
		$("#questions").html($questions.text());
		setQuestionsValue();

		// 視聴後アンケートボタン（リンク先がブランクの時ボタンを非表示）
		// $enquete.attr("href").length && $("#enquete").append(
		// 	$("<a>").attr({
		// 		"href":		$enquete.attr("href"),
		// 		"target":	$enquete.attr("target")
		// 	}).append($('<p>').html(
		// 		$enquete.text()
		// 	))
		// );
		//[buttonBlock]に非表示の input 要素を追加する
		$("#buttonBlock").append(
			$("<input>").attr({
				"type":		"hidden",
				"name":		"linkurl",
				"value":	$enquete.attr("href")
			})
		);

		// ｅ－ＭＲボタン（リンク先がブランクの時ボタンを非表示）
		$product1.find("a").attr("href").length && $("#emrBtn").append(
			$("<a>").attr({
					"href":		$product1.find("a").attr("href"),
					"target":	$product1.find("a").attr("target")
				}).css({
					"background-color": $product1.find("a").attr("color")
				})
				.addClass("colored")
				.html($product1.find("a").text())
			).addClass("buttonProduct01");


		$(".loading").removeClass("loading");
		setEnqueteQuery();

		/* --------------------------------------------------
			style書き出し
		-------------------------------------------------- */
		$xml = $style;
		var style = {
			textSize: 			$xml.find("textSize").text(),
			textColor: 			$xml.find("textColor").text(),

			linkColor: 			$xml.find("linkColor").text(),

			button01Color: 		$xml.find("button01Color").text(),
			button01BgColor: 	$xml.find("button01BgColor").text(),
			button02Color: 		$xml.find("button02Color").text(),
			button02BgColor: 	$xml.find("button02BgColor").text(),

			listSize: 			$xml.find("listSize").text(),
			listColor: 			$xml.find("listColor").text(),

			titleSize: 			$xml.find("titleSize").text(),
			titleColor: 		$xml.find("titleColor").text(),
			titleBgColor: 		$xml.find("titleBgColor").text(),

			scheduleTitleSize: 	$xml.find("scheduleTitleSize").text(),
			scheduleTitleColor:	$xml.find("scheduleTitleColor").text()
		}
		$("#main").css({
			"font-size": 	style.textSize,
			"color": 		style.textColor
		});

		$("a:not(.colored)").css("color", style.linkColor);
		$(".btn02, .btn02vod").css({
			"color": 			style.button01Color,
			"background-color": style.button01BgColor
		});
		$(".btn04, .btn05").css({
			"color": 			style.button02Color,
			"background-color": style.button02BgColor
		});

		$("dl").css({
			"font-size": 	style.listSize,
			"color": 		style.listColor
		});

		$(".title02").css({
			"font-size": 	style.titleSize,
			"color": 		style.titleColor,
			"background-color": style.titleBgColor
		});

		$(".title02 span").css("border-left-color", style.titleColor);

		$(".title03").css({
			"font-size": 	style.scheduleTitleSize,
			"color": 		style.scheduleTitleColor
		});

		$(".title04").css({
			"font-size": 	style.titleSize,
			"color": 		style.titleColor,
			"background-color": style.titleBgColor
		});
	}

 	// Cookieからパラメータを読み込んでHTMLのinput要素に設定する
 	function setQuestionsValue(){
		var qry_uid = getCookie("uid");
		var qry_liveid = getCookie("liveid");
		//console.log("setQuestionsValue: " + qry_liveid);
		if ($("input[name='a0100']").length > 0) {
			$("input[name='a0100']").val(encodeURIComponent(qry_uid));
		}
		if ($("input[name='a0500']").length > 0) {
			$("input[name='a0500']").val(encodeURIComponent(qry_liveid));
		}
 	}

	var setEnqueteQuery = function () {
		// アンケート
        /*
		if ($("#enquete a").size() > 0 && typeof getCookie == "function") {
			var qry_uid = getCookie("uid");
			var qry_liveid = getCookie("liveid");
			var href = $("#enquete a").attr("href");
			//href = href + '?liveid=' + encodeURIComponent(qry_liveid);
			href = href + '?c=liveid:' + encodeURIComponent(qry_liveid);

			$("#enquete a").attr("href", href);
		}
	   */
	}

	var createObj = function($xml) {
		var obj = {
			title: $xml.find("title").text(),
			name: $xml.find("name").text(),
			attention: [],
			dlList: []
		};

		$xml.find("attention").each(function() {
			obj.attention.push($(this).text());
		});

		$xml.find("list").each(function() {
			obj.dlList.push({
				title: $(this).find("listTitle").text(),
				body: $(this).find("listBody").text()
			});
		});

		return obj;
	}

	var writeCommonElement = function(id, obj) {
		var i;

		$(id).find(".sectionTitle").html(obj.title);

		for (i = 0; i < obj.attention.length; i++) {
			$(id).find(".attention").append(
				$("<li>").html(obj.attention[i])
			);
		};

		for (i = 0; i < obj.dlList.length; i++) {
			$(id).find("dl").append(
				$("<dt>").html(obj.dlList[i].title)
			).append(
				$("<dd>").html(obj.dlList[i].body)
			);
		};
	}

	$.ajax({
		type: "GET",
		url: xmlPath + "common/xml/setting.xml",
		dataType: "xml",
		success: main
	});
});