$(function(){
	var qry_uid = getQuerystring("uid");
	var qry_liveid = getQuerystring("liveid");

	
	if ($("input[name='a0100']").size() > 0) {
		$("input[name='a0100']").val(encodeURIComponent(qry_uid));
	}
	if ($("input[name='a0500']").size() > 0) {
		$("input[name='a0500']").val(encodeURIComponent(qry_liveid));
	}
});
function getQuerystring(key, default_){
	if (default_==null) default_="";
	key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
	var qs = regex.exec(window.location.href);
	if(qs == null){
		return default_;
	} else {
		return qs[1];
	}
}

