//------------------------------------------------------------------------------
// form.js
// version 2.0.0
//------------------------------------------------------------------------------
// 基本設定
//------------------------------------------------------------------------------

// BBS-ID
var val_bbs_id		= 385;

// 投稿後、投稿フォームへ戻るまでの時間（秒）
var val_sec		= 3;

// 投稿エラー時、投稿フォームへ戻るまでの時間(秒)
var val_error_sec = 5;

// 一般用投稿フォーム：0、管理者用投稿フォーム：1
var val_admin_flag	= 0;

//------------------------------------------------------------------------------
// CGI設定
//------------------------------------------------------------------------------
// write.cgi
// var cgi_url		= 'http://cgi1.stream.co.jp/cgi-bin/user/write.cgi';

// write.cgi(SSL通信用)
var cgi_url		= 'https://cgi1.stream.co.jp/cgi-bin/user/write.cgi';

// リクエストメソッド
var request_method	= 'GET';

// レスポンスデータタイプ
var response_datatype	= 'jsonp';

//------------------------------------------------------------------------------
// 文言設定
//------------------------------------------------------------------------------

// 投稿成功
var msg_thanks		= '投稿ありがとうございました。';

// システムの問題で投稿に失敗
var msg_syserror	= '投稿に失敗しました。システムエラーが発生しました。';

// 禁止IPアドレスから投稿
var msg_notallowed_ip	= '投稿に失敗しました。許可されていないIPアドレスからアクセスされました。';

// BBSが利用期間外
var msg_outofservice	= '投稿に失敗しました。このBBSは現在ご利用いただけません。';

// 上記以外の理由で投稿に失敗
var msg_other_error	= '投稿に失敗しました。';

// 投稿中
var msg_from_landing = '投稿中です。<br />しばらくお待ちください。';

//------------------------------------------------------------------------------
// フォーム設定
//------------------------------------------------------------------------------
// フォーム情報定義配列
// 送信するパーツの設定を定義します。[ ] は省略可能な項目です。
// ここに定義しないパーツは送信されません。
// 	"name" : フォームパーツのnameを指定します。
// 	["required" : 必須回答の場合は1を指定します。]
// 	["limittype" : 制限タイプを指定します。"max" : 最大入力文字、"min" : 最小入力文字]
// 	["limitcount" : 制限文字数を指定します。]
// 	["limitchar" : 制限文字種別を指定します。"z" : 全角、"h" : 半角]
// 対応パーツは、text, textarea, radio, checkbox, 単一select, hiddenです。それ以外は、非対応です。
var ary_pset = new Array(
	
	{
		"name"		: "a0100"
	},
	{
		"name"		: "a0200"
	},
	{
		"name"		: "a0300"
	},
	{
		"name"		: "a0400"
	},
	{
		"name"		: "a0500"
	}
);
/*var ary_pset = new Array(
	{
		"name"		: "a0100"
	},
	{
		"name"		: "2.ご施設名"
	},
	{
		"name"		: "1.お名前"
	},
	{
		"name"		: "3.内容"
	},
	{
		"name"		: "a0500"
	},
	{
		"name"		: "0.投稿サイト"
	}
);
*/
//------------------------------------------------------------------------------
// 投稿する
//------------------------------------------------------------------------------
function doPost() {
	// エラー表示初期化
	$('.errormsg').remove();

	// 送信前エラーチェック
	var ary_keyval = [];
	var ary_error = [];
	for (var i = 0; i < ary_pset.length; i++)
	{
		var pset = ary_pset[i];
		var parts = $('[name = "' + pset.name + '"]');
		
		// 定義してあるパーツが存在しない場合は無視して次へ
		if (parts[0] == undefined)
		{
			continue;
		}
		
		// 値の取得
		var pvalue;
		switch (parts[0].type) 
		{
			case 'text':
				pvalue = parts.val();
				break;
			case 'textarea':
				pvalue = parts.val();
				break;
			case 'radio':
				pvalue = parts.filter(':checked').val();
				if (pvalue == undefined) { pvalue = ''; } // 未選択時：undefined
				break;
			case 'checkbox':
				pvalue = parts.filter(':checked').val();
				if (pvalue == undefined) { pvalue = ''; } // 未選択時：undefined
				break;
			case 'select-one':
				pvalue = parts.val();
				break;
			case 'hidden':
				pvalue = parts.val();
				break;
			default:
				ary_error.push([parts, '未対応のパーツです。']);
				break;
		}
		
		// 必須チェック
		if (pset.required)
		{
			var x;
			if (!pvalue)
			{
				ary_error.push([parts, '回答必須です。']);
			}
		}
		
		// 制限チェック
		if (pset.limittype)
		{
			var limitbytes;
			switch (pset.limitchar)
			{
				case 'h':
					charja = '半角';
					limitbytes = pset.limitcount;
					break;
				case 'z':
					charja = '全角';
					limitbytes = pset.limitcount * 2;
					break;
				default:
					ary_error.push([parts, '未定義の文字種別です。']);
					break;
			}
			
			var pbytes = get_bytes(pvalue);
			switch (pset.limittype)
			{
				case 'max':
					if (pbytes > limitbytes)
					{
						ary_error.push([parts, charja + pset.limitcount + '文字以下で回答してください。']);
					}
					break;
				case 'min':
					if (pbytes < limitbytes)
					{
						ary_error.push([parts, charja + pset.limitcount + '文字以上で回答してください。']);
					}
					break;
				default:
					ary_error.push([parts, '未定義の制限種別です。']);
					break;
			}
		}
		
		pvalue = json_escape(pvalue);
		var keyval = '"' + pset.name + '":"' + pvalue + '"';
		ary_keyval.push(keyval);
	}
	ary_keyval.push('"bbs_id":"' + val_bbs_id + '"');
	ary_keyval.push('"admin_flag":"' + val_admin_flag + '"');
	
	// エラー表示
	if (ary_error.length)
	{
		for (var i = 0; i < ary_error.length; i++)
		{
			var parts = ary_error[i][0];
			var errormsg = ary_error[i][1];
			
			parts.parent().before(error_tag(errormsg));
		}
		return;
	}
	
	var str_json = '{' + ary_keyval.join(",") + '}';
	str_json = encodeURIComponent(str_json);
	
	// 送信する
	$.ajax({
		url		: cgi_url,
		beforeSend	: function(xhr, settings) {
			var ver = $.browser.version;
			settings.url.match(/^.+?\/\/.+?(\/.+)$/);
			var abs_url = RegExp.$1;
			var request_str = 'GET ' + abs_url + ' HTTP/1.1';
			
			var error_msg;
			if ($.browser.msie && (6 <= ver && ver <= 8))
			{
				// IEの上限
				if (request_str.length > 2000)
				{
					error_msg = '投稿可能な上限バイト数をオーバーしています。（上限：2000バイト、現在：' + request_str.length + 'バイト）';
				}
			}
			else
			{
				// サーバーの上限
				if (request_str.length > 8190)
				{
					error_msg = '投稿可能な上限バイト数をオーバーしています。（上限：8190バイト、現在：' + request_str.length + 'バイト）';
				}
			}
			
			if (error_msg)
			{
				$('#form').before(error_tag(error_msg));
				return false;
			}
			else
			{
				$('#form').html(msg_from_landing);
			}
		},
		type		: request_method,
		data		: str_json,
		scriptCharset	: 'utf-8',
		error	: function(request, status, error) {
			var output	= get_msg(4001);
			$('#form').html(error_tag(output));
			setTimeout('location.reload()', val_error_sec * 1000);
		},
		success		: function(json) {
			var ret_code	= parseInt(json.code, 10);
			var output	= get_msg(ret_code);
			if (ret_code == 2000)
			{
				$('#form').html(output);
				setTimeout('location.reload()', val_sec * 1000);
			}
			else
			{
				$('#form').html(error_tag(output));
				setTimeout('location.reload()', val_error_sec * 1000);
			}
		},
		dataType	: response_datatype
	})
}

//------------------------------------------------------------------------------
// 引数の文字列のバイト数取得を取得する
//------------------------------------------------------------------------------
function get_bytes(str) {
	var r = 0;
	for (var i = 0; i < str.length; i++)
	{
		var c = str.charCodeAt(i);
		// Shift_JIS: 0x0 ～ 0x80, 0xa0 , 0xa1 ～ 0xdf , 0xfd ～ 0xff
		// Unicode : 0x0 ～ 0x80, 0xf8f0, 0xff61 ～ 0xff9f, 0xf8f1 ～ 0xf8f3
		if ( (c >= 0x0 && c < 0x81) || (c == 0xf8f0) || (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4))
		{
			r += 1;
		} else
		{ 
			r += 2;
		}
	}
	return r;
}

//------------------------------------------------------------------------------
// 引数の文字列でエラーメッセージ用のタグを返却する
//------------------------------------------------------------------------------
function error_tag(str) {
	return '<li class="errormsg">[!]' + str + '</li>';
}

//------------------------------------------------------------------------------
// 予約語のエスケープ処理をする
//------------------------------------------------------------------------------
function json_escape(val) {
	val = val.replace(/\r|\n/g, '<br />');
	val = val.replace(/\t/g, ' ');
	val = val.replace(/\\/g, '\\\\');
	val = val.replace(/\"/g, '\\\"');
	return val;
}


//------------------------------------------------------------------------------
// 引数のコードに対応した文言を返却する
//------------------------------------------------------------------------------
function get_msg(ret_code) {
	var val_msg;
	switch(ret_code)
	{
		case 2000:
			val_msg = msg_thanks;
			break;
		case 4001:
			val_msg = msg_syserror;
			break;
		case 4002:
			val_msg = msg_notallowed_ip;
			break;
		case 4003:
			val_msg = msg_outofservice;
			break;
		default:
			val_msg = msg_other_error;
	}
	return val_msg;
}

