$(function(){
	var host = location.href;
	var qry = $.url(host).param();
	var quyObj = Object.assign({}, qry);

	if ($("a.enquete").size() > 0) {
		var href = $("a.enquete").attr("href");
		var sep = "?";
		if (href.indexOf("?") != -1) {
			if (href.split("?")[1] == "") sep = "";
			else sep = "&";
		}

		if (qry.p) {
			href += sep + "c=" + encodeURIComponent(qry.p);
		}
		$("a.enquete").attr("href", href);
	}

	if ($("#player").size() > 0) {
		var src = $("#player").attr("src");
		if (qry.p) {
			src = src.replace(/###set-qry###/g, encodeURIComponent("?c=" + qry.p));
		} else {
			src = src.replace(/###set-qry###/g, "");
		}
		$("#player").attr("src", src);
	}

	// TODO: 複数のパラメータに対応 202209
	// 集客媒体コード追加
	quyObj['medium_code'] = window.medium_code;

	if ($("#bbs").size() > 0) {
		var src = $("#bbs").attr("src");
		var sep = "?";

		if(Object.keys(quyObj).length) {
			Object.keys(quyObj).forEach(function(key, index){
				if (src.indexOf("?") != -1) {
					if (src.split("?")[1] == "") sep = "";
					else sep = "&";
				}
				src += sep + key +"=" + encodeURIComponent(quyObj[key]);
			})
		}
		$("#bbs").attr("src", src);
	}
	// if ($("form[id='form'] input[name='a0500']").size() > 0) {
	// 	if (qry.p) {
	// 		$("form[id='form'] input[name='a0500']").val(encodeURIComponent(qry.p));
	// 	}
	// }

	// リアルタイムアンケート
	// TODO: 順番は調整するかも
	if ($("#vote").size() > 0) {
		var src = $("#vote").attr("src");
		var sep = "?";

		if(Object.keys(quyObj).length) {
			Object.keys(quyObj).forEach(function(key, index){
				if (src.indexOf("?") != -1) {
					if (src.split("?")[1] == "") sep = "";
					else sep = "&";
				}
				src += sep + 'op' + (index + 1) +"=" + encodeURIComponent(quyObj[key]);
			})
		}
		$("#vote").attr("src", src);
	}

	if ($("#enq").size() > 0) {
		var src = $("#enq").attr("src");
		if (qry.p) {
			src = src.replace(/###set-qry###/g, encodeURIComponent("?c=" + qry.p));
		} else {
			src = src.replace(/###set-qry###/g, "");
		}
		$("#enq").attr("src", src);
	}

});