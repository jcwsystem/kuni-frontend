# 【鳥居薬品】扉ページ・視聴ページ制作：2022年08月

##
■DEV JCW
- https://dev.j-creativeworks.net/torii-live/202210/html/
- torii / live

■Git
- https://bitbucket.org/jcwsystem/individual-project/src/master/

■local
- C:\Users\t.kunishima\Desktop\HOMEWORKS\t_鳥居

■backlog
- https://jcwseisak.backlog.jp/view/CS_PJ-1506


■ローカルページ
- [202210](./www/202210/html/index.html "202210")

### 仕様
- ライブページ作成
  - ログインページ
    - PWはハッシュ化、xml管理
    - 演題と開催日時はxml管理
    - ログワークスで芳名録を保存
  - 扉ページ
    - ボタンに時限設定 xml管理
    - 演題と開催日時はxml管理
    - 講演者部分は全てカット
  - 視聴ページ
    - ログワークスでログインページの入力情報とページID（dvr,live）をログ


### 作業環境
- webpack

### 参考に出来そうなもの


### 振り返り

------

