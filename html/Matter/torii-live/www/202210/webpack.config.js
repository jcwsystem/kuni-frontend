const path = require('path');
const webpack = require('webpack');

module.exports = {
  // モード値を production に設定すると最適化された状態で、
  // development に設定するとソースマップ有効でJSファイルが出力される
  mode: 'development',

  devServer: {
    contentBase: path.resolve(__dirname, 'html'),
    open: true
  },

  module: {
   rules: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader",
      options: {
        presets: [
          [
            '@babel/preset-env',
            {
              'useBuiltIns': 'usage',
              // 'modules': false,
              // 'targets': {'browsers': ['ie >= 11']}
            }
          ],

          "@babel/preset-react"
        ]
      }
    }
   ]
  },

  // plugins: [
  //   new webpack.ProvidePlugin({
  //     $: 'jquery',
  //     jQuery: 'jquery'
  //   })
  // ],

  // メインとなるJavaScriptファイル（エントリーポイント）
  // entry: './src/index.js',
  entry: {
    'viewport': './src/viewport.js',
    'index': './src/index.js',
    'auth_page': './src/auth_page.js',
    'player': './src/player.js',
    'switch': './src/switch.js',
  },

  // ファイルの出力設定
  output: {
    //  出力ファイルのディレクトリ名
    path: path.resolve(__dirname, 'html'),
    // 出力ファイル名
    filename: 'js/[name].js'
  },

  devtool: 'source-map'
};
