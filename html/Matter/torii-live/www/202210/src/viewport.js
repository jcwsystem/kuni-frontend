(function() {



var wideWidth = 1020;
var isThis = new function() {
	this.ua = navigator.userAgent;
	
	this.iPhone = this.ua.indexOf("iPhone") != -1;
	this.iPad = this.ua.indexOf("iPad") != -1;

	this.Android = this.ua.indexOf("Android") != -1;
	this.AndroidSmartPhone = this.Android && this.ua.indexOf("Mobile") != -1 && this.ua.indexOf("A1_07") == -1 && this.ua.indexOf("SC-01C") == -1;
	this.AndroidTablet = this.Android && (this.ua.indexOf("Mobile") == -1 || this.ua.indexOf("A1_07") != -1 || this.ua.indexOf("SC-01C") != -1);
	
	this.sp = this.iPhone || this.AndroidSmartPhone;
	this.tablet = this.iPad || this.AndroidTablet;
};

if (isThis.sp) {
	document.write('<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">');
} else {
	document.write('<meta name="viewport" content="width=' + wideWidth + '">');
}



})();
