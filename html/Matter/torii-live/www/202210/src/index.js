import $ from "jquery";
import Cookies from "js-cookie";
import config from "./config";
import crypto from "crypto";
import pageConfig from "./page_config";
import GetOpenStatus from "./modules/get_open_status";
import SetParmForcedMode from "./modules/set_parm_forced_mode";

const target_day = pageConfig.target_day;
const user = {};
const required = config.required;
const page = config.page;
const errorMessage = config.errorMessage;

// 公開モード 公開前
const DISP_MODE_BEFORE = pageConfig.DISP_MODE_BEFORE;
// 公開モード 公開中
const DISP_MODE_OPEN = pageConfig.DISP_MODE_OPEN;
// 公開モード 公開終了
const DISP_MODE_CLOSE = pageConfig.DISP_MODE_CLOSE;

const CHECK_TIMER = pageConfig.CHECK_TIMER;

$(() => {

	let checkTime = function() {
		return $.when(
      $.ajax({
        type: "GET",
        url: './xml/setting.xml',
        dataType: "xml",
        cache: false,
      }),

      // DOM待ち
      $.ready

    ).then( result => {
      const [xml, status, xhr] = result;

      // XMLヘッダーからサーバーの現在時刻取得
      const currentDate = xhr.getResponseHeader("date");

      // ライブの情報を取得
      const $live_setting = $(xml).find(`live_setting[Day="${target_day}"]`);
      const $pw_any = $(xml).find('pw_any');

      if ($live_setting.length) {
        const open = $live_setting.find("loginopentime").text();
        const close = $live_setting.find("loginclosetime").text();
        const openStatus = GetOpenStatus(currentDate, open, close);

				const $submit = $("#submit");
        switch (openStatus) {
          case DISP_MODE_BEFORE:
            $submit.attr("aria-disabled", "true");
            break;
          case DISP_MODE_OPEN:
            $submit.attr("aria-disabled", "false");
            break;
          case DISP_MODE_CLOSE:
            $submit.attr("aria-disabled", "true");
            break;
        }

        SetParmForcedMode('login');

				// console.log("checkTIme")
				return new $.Deferred().resolve($live_setting, $pw_any).promise();
      }

    }, (err) => {
			const [xhr, status, error] = err;
			console.error('AJAX通信エラー(XML)', xhr, status, error);
		})
	};

	checkTime().then((result, result2) => {

		// const $pw_anys = $(result2).find('any');
		const $pw_anys = $(result2).attr('keys').replace(/\s/g, '').split(',').filter(Boolean);
		const newRequired = Object.assign([], required);
		// console.log("$pw_anys", $pw_anys, $pw_anys.length)
		// 必須項目を決定
		if($pw_anys.length) {
			$.each($pw_anys, (_, key) => {
				// console.log("$(key).text()", $(key).text())
				// console.log("required.indexOf($(key).text())", newRequired.indexOf($(key).text()))
				// console.log("newRequired", newRequired)
				newRequired.splice(newRequired.indexOf(key), 1);
			});
		}

		// 必須項目にclass追加
		$.each(newRequired, (_, item) => {
			$(`[data-required="${item}"]`).addClass('isRequired');
		});

		// console.log('newRequired', newRequired)
		// クッキーリセット
		required.forEach(required => {
			Cookies.remove(required);
		});

		const $live_setting = $(result);
		const sha256PassWord = $live_setting.find("password").text();

		const siteName = $live_setting.find("siteName").text();
		const deliveryDate = $live_setting.find("deliveryDate").text();

		$('[data-seminar="siteName"]').text(siteName);
		$('[data-seminar="deliveryDate"]').text(deliveryDate);

		$("#submit").on('click', (e) => {
			e.preventDefault();

			let requiredCheck = true;

			$("#info").empty();

			required.forEach(required => {
				const $element = $('[name="' + required + '"]');

				if ($element.length > 1) {
					if ($element.attr("type") == "radio") {
						user[required] = $('[name="' + required + '"]:checked').val();
					}

				} else {
					user[required] = $element.val();
				}


				if (newRequired.includes(required) && !user[required]) {
					requiredCheck = false;
				}
			});

			// 必須項目フラグチェック
			if (!requiredCheck) {
				$("#info").html(errorMessage.required);
				return false;
			}

			// 入力チェック
			for (const key in user) {
				// const value = user[key];

				// 正規表現チェックが必要なものはチェック
				// if (match[key] && !match[key].exec(value)) {
				// 	$("#info").html(errorMessage[key]);
				// 	return false;
				// }
				// sha256ハッシュ化したパスワードチェック
				if(key === "password") {
					const password = $("#password").val();
					const hash = crypto.createHash("sha256").update(password).digest("hex");
					if (hash !== sha256PassWord) {
						$("#info").html(errorMessage.password);
						return false;
					}

					// console.log("required", required)
					user[required]
				}


			}
			// console.log("user", user)

			// クッキー書き込み
			required.forEach(required => {
				if(required == "password"){
					return true;
				}

				// console.log("user[required]", required, user[required])
				const Value = user[required] ? user[required] : '_';
				Cookies.set(required, Value, {expires: 1});
			});
			// console.log("user", user)


			// 強制閲覧モードの継承
			if(/\?forced_mode\=2/.test(location.href)) {
				location.href = page.detail+'?forced_mode=2';
			}else{
				location.href = page.detail;
			}
			// location.href = page.detail;
		});

		// パスワードの表示切り替え
		$('[data-toggle-pw]').on('click', (e) => {
			const pw = $('#password');
			if(pw.attr('type') === 'password') {
				pw.attr('type', "text");
				$(e.target).attr("src", (_, src) => {
					return src.replace("_on", "_off");
				})
			}else{
				pw.attr('type', "password");
				$(e.target).attr("src", (_, src) => {
					return src.replace("_off", "_on");
				})
			}
		});
	})

	setInterval(function () {
		checkTime();
	}, CHECK_TIMER);

	// $.ajax({
	// 	type: 'GET',
	// 	url: './xml/setting.xml',
	// 	dataType: 'xml',
	// 	cache: false,
	// })
	// .then(result => {
	// 	const live_setting = $(result).find("live_setting");
	// 	const sha256PassWord = $(result).find("password").text();

	// 	const siteName = live_setting.find("siteName").text();
	// 	const deliveryDate = live_setting.find("deliveryDate").text();

	// 	$('[data-seminar="siteName"]').text(siteName);
	// 	$('[data-seminar="deliveryDate"]').text(deliveryDate);


	// 	$("#submit").on('click', (e) => {
	// 		e.preventDefault();

	// 		let requiredCheck = true;

	// 		$("#info").empty();

	// 		required.forEach(required => {
	// 			const $element = $('[name="' + required + '"]');

	// 			if ($element.length > 1) {
	// 				if ($element.attr("type") == "radio") {
	// 					user[required] = $('[name="' + required + '"]:checked').val();
	// 				}

	// 			} else {
	// 				user[required] = $element.val();
	// 			}

	// 			if (!user[required]) {
	// 				requiredCheck = false;
	// 			}

	// 		});

	// 		// 必須項目フラグチェック
	// 		if (!requiredCheck) {
	// 			$("#info").html(errorMessage.required);
	// 			return false;
	// 		}

	// 		// 入力チェック
	// 		for (const key in user) {
	// 			// const value = user[key];

	// 			// 正規表現チェックが必要なものはチェック
	// 			// if (match[key] && !match[key].exec(value)) {
	// 			// 	$("#info").html(errorMessage[key]);
	// 			// 	return false;
	// 			// }
	// 			// sha256ハッシュ化したパスワードチェック
	// 			if(key === "password") {
	// 				const password = $("#password").val();
	// 				const hash = crypto.createHash("sha256").update(password).digest("hex");
	// 				if (hash !== sha256PassWord) {
	// 					$("#info").html(errorMessage.password);
	// 					return false;
	// 				}
	// 				user[required]
	// 			}


	// 		}

	// 		// クッキー書き込み
	// 		required.forEach(required => {
	// 			if(required == "password"){
	// 				return true;
	// 			}
	// 			Cookies.set(required, user[required], {expires: 1});
	// 		});

	// 		location.href = page.detail;
	// 	});
	// })


});
