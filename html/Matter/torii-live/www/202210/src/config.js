const config = {
	required: ["password","user_name","affiliation","pref_name","occupation","department"],
	match: {
		//userid: /^[!-~]{8,8}$/
	},

	page: {
		index: "index.html",
		detail:	"detail.html",
		player:	"live.html"
	},

	player_dir :	"../../",
	xml_dir :	"./xml/",

	errorMessage: {
		required: "必須項目を入力してください。",
		password: "パスワードが違います。",
		user_name: "名前が不正な値です。",
		affiliation: "所属組織が不正な値です。",
		pref_name: "都道府県が不正な値です。"
	}
};

export default config;