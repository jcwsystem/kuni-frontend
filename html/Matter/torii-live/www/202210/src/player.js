import $ from "jquery";
import Cookies from "js-cookie";
import config from "./config";

const required = config.required;
const page = config.page;
const cookieValues = {};

required.forEach(required => {
	const value = Cookies.get(required);
	if(required !== 'password') {
		if (value) {
			cookieValues[required] = value;
		} else {
			location.href = config.player_dir + page.index;
		}
	}
});

window.cookieValues = cookieValues;

