import $ from "jquery";

import pageConfig from "../page_config";

export default function setParmForcedMode(type) {
  // 公開モード 公開前
  const DISP_MODE_BEFORE = pageConfig.DISP_MODE_BEFORE;
  // 公開モード 公開中
  const DISP_MODE_OPEN = pageConfig.DISP_MODE_OPEN;
  // 公開モード 公開終了
  const DISP_MODE_CLOSE = pageConfig.DISP_MODE_CLOSE;

  // 強制参照モード
  const FORCED_BROWSE_KEY = pageConfig.FORCED_BROWSE_KEY;

  // 強制参照モードの引数を追加セットするリンクに以下classを設定
  const FORCED_BROWSE_ADD_CLASS = pageConfig.FORCED_BROWSE_ADD_CLASS;

  // 強制閲覧モード
  let forcedMode = Number(paramGetQuerystring(FORCED_BROWSE_KEY));

  switch (type) {
    case "pages":
      // 存在しないモードの場合、スルー
      if (
        !(
          forcedMode === DISP_MODE_BEFORE ||
          forcedMode === DISP_MODE_CLOSE ||
          forcedMode === DISP_MODE_OPEN
        )
      )
        return;

      $("." + FORCED_BROWSE_ADD_CLASS).attr("href", function (i, href) {
        var params = FORCED_BROWSE_KEY + "=" + forcedMode;

        return href.replace(/\?.*/g, "") + "?" + params;
      });
      break;

    case "login":
      // 存在しないモードの場合、スルー
      if (!(forcedMode === DISP_MODE_OPEN)) return;

      // submit用の処理
      $("#submit").attr("aria-disabled", "false");
      break;
  }

  function paramGetQuerystring(key) {
    key = key.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null) {
      return false;
    } else {
      return qs[1];
    }
  }
}