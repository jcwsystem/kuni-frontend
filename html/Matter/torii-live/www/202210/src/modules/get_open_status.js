import pageConfig from "../page_config";

export default function getOpenStatus(now, open, close) {
    // 公開モード 公開前
    const DISP_MODE_BEFORE = pageConfig.DISP_MODE_BEFORE;
    // 公開モード 公開中
    const DISP_MODE_OPEN = pageConfig.DISP_MODE_OPEN;
    // 公開モード 公開終了
    const DISP_MODE_CLOSE = pageConfig.DISP_MODE_CLOSE;

    const ERR_DATE_STR = pageConfig.ERR_DATE_STR;
    // 強制参照モード
    const FORCED_BROWSE_KEY = pageConfig.FORCED_BROWSE_KEY;

    var chkTimeFormat = function(str) {
      if (
        typeof str === "string" &&
        (str.match(/^\d{4}\/\d{2}\/\d{2}\s\d{2}\:\d{2}\:\d{2}$/) ||
          str.match(
            /^(\d{1,4})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])T([0-1][0-9]|2[0-4]):[0-5][0-9]:[0-5][0-9](Z|(\+|-)([01][0-9]|2[0-4]):?([0-5][0-9])?)$/
          ))
      ) {
        return true;
      }
      return false;
    };

    /**
   * クエリパラメータ取得
   * @param string key
   */
    var paramGetQuerystring = function (key) {
      key = key.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
      var qs = regex.exec(window.location.href);
      if (qs == null) {
        return false;
      } else {
        return qs[1];
      }
    };

    // 時間フォーマットが不正の場合
    if (
      !(now && open && chkTimeFormat(open) && close && chkTimeFormat(close))
    ) {
      return DISP_MODE_BEFORE;
    }

  // 強制閲覧モードチェック
  const forcedMode = Number(paramGetQuerystring(FORCED_BROWSE_KEY));

  // 設定あった場合
  if (forcedMode) {
    // 存在するモードなら、そのまま返却
    if (
      [
        DISP_MODE_BEFORE,
        DISP_MODE_CLOSE,
        DISP_MODE_OPEN
      ].includes(forcedMode)
    ) {
      return forcedMode;
    }
  }

  // 時差表記の文字列で処理するのでnew Date使用で大丈夫
  const nowTime = new Date(now);
  const openTime = new Date(open);
  const closeTime = new Date(close);

  if (
    nowTime == ERR_DATE_STR ||
    openTime == ERR_DATE_STR ||
    closeTime == ERR_DATE_STR
  ) {
    return DISP_MODE_BEFORE;
  }

  const nowMoment = nowTime.getTime();
  const openMoment = openTime.getTime();
  const closeMoment = closeTime.getTime();

  if (Number.isNaN(nowMoment) ||
    Number.isNaN(openMoment) ||
    Number.isNaN(closeMoment)) {
    return DISP_MODE_BEFORE;
  }

  // 公開前
  if (nowMoment < openMoment) {
    return DISP_MODE_BEFORE;
  }

  // 公開終了
  if (nowMoment >= closeMoment) {
    return DISP_MODE_CLOSE;
  }

  // 公開中
  return DISP_MODE_OPEN;

}