const pageConfig = {
		target_day: "1",
    // 公開モード 公開前
    DISP_MODE_BEFORE: 1,
    // 公開モード 公開中
    DISP_MODE_OPEN: 2,
    // 公開モード 公開終了（DVRのみページ視聴可能の全工程終了）
    DISP_MODE_CLOSE: 3,

		ERR_DATE_STR: "Invalid date",

		// 対象ページ TOP
		TARGET_PAGE_TOP: "TOP",
		// 対象ページ Player
		TARGET_PAGE_PLAYER: "PLAYER",

		TARGET_PLAYER_DIR: "../../",

		SETTING_XML_PATH: "xml/setting.xml",

		// 強制参照モード
		FORCED_BROWSE_KEY: "forced_mode",

		// 強制参照モードの引数を追加セットするリンクに以下classを設定
		FORCED_BROWSE_ADD_CLASS: "add_forced_mode",

		CHECK_TIMER: 60000,
};

export default pageConfig;