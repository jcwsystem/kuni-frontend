import $ from "jquery";
// import Cookies from "js-cookie";
// import qs from "querystringify";
// import Url from "url-parse";

import pageConfig from "./page_config";
import GetOpenStatus from "./modules/get_open_status";
import SetParmForcedMode from "./modules/set_parm_forced_mode";

/* ----------------------------------------
	初期設定
---------------------------------------- */
// ライブ対象日時(1 or 2)
// HTML側で設定すること
// ※今回は1固定
const target_day = pageConfig.target_day;
// 公開モード 公開前
const DISP_MODE_BEFORE = pageConfig.DISP_MODE_BEFORE;
// // 公開モード 公開中
// const DISP_MODE_OPEN = pageConfig.DISP_MODE_OPEN;
// // 公開モード 公開終了
// const DISP_MODE_CLOSE = pageConfig.DISP_MODE_CLOSE;

// 対象ページ TOP
const TARGET_PAGE_TOP = pageConfig.TARGET_PAGE_TOP;
// 対象ページ Player
const TARGET_PAGE_PLAYER = pageConfig.TARGET_PAGE_PLAYER;

const TARGET_PLAYER_DIR = pageConfig.TARGET_PLAYER_DIR;

const SETTING_XML_PATH = pageConfig.SETTING_XML_PATH;

// // 強制参照モード
// const FORCED_BROWSE_KEY = pageConfig.FORCED_BROWSE_KEY;

// // 強制参照モードの引数を追加セットするリンクに以下classを設定
// const FORCED_BROWSE_ADD_CLASS = pageConfig.FORCED_BROWSE_ADD_CLASS;

const CHECK_TIMER = pageConfig.CHECK_TIMER;

// ****************
// 切り替え用 関数
// ****************
window.switchModel = (function () {
  // ************************************************
  // privateモジュール
  // ************************************************
  /**
   * 時間チェック（メイン処理）
   */
  var checkTime = function (target_page) {

    // plyaerページの場合、階層設定
    var xmlDir = target_page == TARGET_PAGE_PLAYER ? TARGET_PLAYER_DIR : "";
    xmlDir += SETTING_XML_PATH;

    // xml取得
    return $.when(
      $.ajax({
        type: "GET",
        url: xmlDir,
        dataType: "xml",
        cache: false,
      }),

      // DOM待ち
      $.ready

    ).then(function (result) {
			const [xml, status, xhr] = result;

			// XMLヘッダーからサーバーの現在時刻取得
			const currentDate = xhr.getResponseHeader("date");

      // ライブの情報を取得
			const $live_setting = $(xml).find(`live_setting[Day="${target_day}"]`);
      if ($live_setting.length) {
        var open = $live_setting.find("opentime").text();
        var close = $live_setting.find("closetime").text();

        // 公開期間を元に公開ステータス取得
        var openStatus = GetOpenStatus(currentDate, open, close);
        // console.log("openStatus", openStatus)
        // TOPページの場合
        if (target_page == TARGET_PAGE_TOP) {
          switchTop(openStatus, $live_setting);

          // PLAYERページの場合
        } else if (target_page == TARGET_PAGE_PLAYER) {
          switchPlayer(openStatus);
        }

        // 各種リンクに強制閲覧モードセット
        SetParmForcedMode('pages');

        return new $.Deferred().resolve($live_setting).promise();
      }

    })
    .fail(function (result) {
			const [xhr, status, error] = result;
			console.error('AJAX通信エラー(XML)', xhr, status, error);
    });
  };

  /**
   * 表示切替（TOPページ用）
   */
  var switchTop = function (openStatus, live_setting) {


    // 現在の公開ステータスに合う公開設定から出力内容切替
    live_setting.find("open_info").each(function () {
      if ($(this).attr("Status") == openStatus) {
        // 表示切替
        var linkHtml = $(this).find("pleyer_btn_link").text();
        var descriptionHtml = $(this).find("pleyer_btn_under_dist").text();
        $("#player_link").html(linkHtml);
        $("#player_description").html(descriptionHtml);

        // each抜け
        return false;
      }
    });
  };

  /**
   * 表示切替（Playerページ用）
   * 公開するまでTOPへリダイレクト
   */
  var switchPlayer = function (openStatus) {
    // if (openStatus == DISP_MODE_BEFORE) {
    //   location.replace(TARGET_PLAYER_DIR + "detail.html");
    //   return;
    // }
  };

  /**
   * Date形式チェック
   * @param string date
   */
  // var isInstanceOfDate = function (date) {
  //   return date instanceof Date && !isNaN(date.valueOf());
  // };
  let getCookie = function(c){
    var result = null;

    var cname = c + '=';
    var allcookies = document.cookie;

    var position = allcookies.indexOf(cname);
    if( position != -1 ){
      var st = position + cname.length;
      var ed = allcookies.indexOf( ';', st );
      if( ed == -1 ){
        ed = allcookies.length;
      }
      result = decodeURIComponent(allcookies.substring(st, ed));
    }

    return result;
  }


  // ************************************************
  // publicモジュール
  // ************************************************
  return {
    // ************************************************
    // TOP画面用 切替処理
    // ************************************************
    initTop: function () {

      // 時間チェック
      checkTime(TARGET_PAGE_TOP).then(function($live_setting) {
        // console.log("$live_setting", $live_setting)

        const siteName = $live_setting.find("siteName").text();
        const seminarTitle = $live_setting.find("seminarTitle").text();
        const seminarSubTitle = $live_setting.find("seminarSubTitle").text();
        const deliveryDate = $live_setting.find("deliveryDate").text();

        // let titleTag = $('title').text();
        // $('title').text(titleTag.replace('||', '| ' + siteName + ' |'));
        // $('[data-seminar="siteName"]').text(siteName);
        $('#startDate').text(deliveryDate);

        $('[data-seminar="siteName"]').text(siteName);
        $('[data-seminar="seminarTitle"]').text(seminarTitle);
        $('[data-seminar="seminarSubTitle"]').text(seminarSubTitle);
        $('[data-seminar="deliveryDate"]').text(deliveryDate);
      });

      setInterval(function () {
        checkTime(TARGET_PAGE_TOP);
      }, CHECK_TIMER);
    },
    // ************************************************
    // PLAYER画面用 切替処理
    // ************************************************
    initPlayer: function () {

      // 時間チェック
      checkTime(TARGET_PAGE_PLAYER).then(function($live_setting) {
        // console.log("$live_setting", $live_setting)
        const siteName = $live_setting.find("siteName").text();
        const deliveryDate = $live_setting.find("deliveryDate").text();

        // let titleTag = $('title').text();
        // console.log(titleTag)
        // $('title').text(titleTag.replace('||', '| ' + siteName + ' |'));
        $('[data-seminar="siteName"]').text(siteName);
        $('#startDate').text(deliveryDate);

        let qryObj = {
          user_name: getCookie("user_name"),
          affiliation: getCookie("affiliation"),
          pref_name: getCookie("pref_name"),
          occupation: getCookie("occupation"),
          department: getCookie("department"),
        }

        $("#normalEnquete").attr('href', function(e) {
          let newHref = Object.keys(qryObj).map(function(key, index) {
            let qry = "";
            let prefix = index === 0 ? '?' : '&';

            qry = prefix + 'op' + Number(index + 1)+ '=' +  encodeURIComponent(qryObj[key]);
            return qry;
          }).filter(Boolean).join('')

          return $("#normalEnquete").attr('href') + newHref;
        });

      });
      setInterval(function () {
        checkTime(TARGET_PAGE_PLAYER);
      }, CHECK_TIMER);
    },
  };
})();
