$(function(){
	var qry_userid = getCookie("userid");

	// BBS
	if ($("#bbs").size() > 0) {
		var src = $("#bbs").attr("src");
		src = src + '?userid=' + encodeURIComponent(qry_userid);
		
		$("#bbs").attr("src", src);
	}
	function getCookie(c){
		var result = null;
	
		var cname = c + '=';
		var allcookies = document.cookie;
	
		var position = allcookies.indexOf(cname);
		if( position != -1 ){
			var st = position + cname.length;
			var ed = allcookies.indexOf( ';', st );
			if( ed == -1 ){
				ed = allcookies.length;
			}
			result = decodeURIComponent(allcookies.substring(st, ed));
		}
	
		return result;
	}
});