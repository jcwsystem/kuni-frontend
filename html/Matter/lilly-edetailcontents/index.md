# リリー e-detailコンテンツ

## 肺がん領域
■DEV JCW  
- https://dev.j-creativeworks.net/lilly_edetail_contents_lc/lilly_veeva_vod/authdir/event/lc_list/
- https://dev.j-creativeworks.net/lilly_edetail_contents_lc/lilly_veeva_vod/authdir/event/player/movie/

■Git  
- https://bitbucket.org/jcwsystem/lilly_edetail_contents_lc/src/master/

■local  
- C:\Users\t.kunishima\Desktop\HOMEWORKS\l_リリーe-detail 動画一覧ページ開発\app_lc

### 仕様
#### コンテンツ一覧ページ
- タグ単一検索
- NEWラベル
- ブランド名, 適応症名ラベル
- PDF, WEBラベル
#### プレイヤーページ
- ブランドでスタイル変更あり

------

## ON領域
■DEV JCW  
- https://dev.j-creativeworks.net/lilly_edetail_contents_on/lilly_veeva_vod/authdir/event/player/movie/
- https://dev.j-creativeworks.net/lilly_edetail_contents_on/lilly_veeva_vod/authdir/event/fbone_list/
- https://dev.j-creativeworks.net/lilly_edetail_contents_on/lilly_veeva_vod/authdir/event/rhead_list/
- https://dev.j-creativeworks.net/lilly_edetail_contents_on/lilly_veeva_vod/authdir/event/ehead_list/

■Git  
- https://bitbucket.org/jcwsystem/lilly_edetail_contents_on

■local  
- C:\Users\t.kunishima\Desktop\HOMEWORKS\l_リリーe-detail 動画一覧ページ開発\app_on

### 仕様
#### コンテンツ一覧ページ
- タグ単一検索
- NEWラベル
- ブランド名ラベル
- ブランド「フォルテオ」のとき、
- ブランド「フォルテオ」のとき、ラベル名が長いものがあるため、css調整をしている。（他のものでも対応すべきか。。）
#### プレイヤーページ
- ブランドでスタイル変更あり
- ブランド「フォルテオ」のとき、一覧ページ・マイページへのリンク・Myリストチェックボタンを非表示。

------

## 糖尿病（DMG）領域
■DEV JCW  
- https://dev.j-creativeworks.net/lilly_edetail_contents_dmg/lilly_veeva_vod/authdir/event/dmg_list/
- https://dev.j-creativeworks.net/lilly_edetail_contents_dmg/lilly_veeva_vod/authdir/event/player/movie/

■Git  
- https://bitbucket.org/jcwsystem/lilly_edetail_contents_dmg

■local  
- C:\Users\t.kunishima\Desktop\HOMEWORKS\l_リリーe-detail 動画一覧ページ開発\app_dmg

### 仕様
#### コンテンツ一覧ページ
- タグ複数検索
- NEWラベル
- ブランド名ラベル
- ブラックリスト機能（特定のブランド名を表示しないようにする）
- 特定のコンテンツをリストに表示させない処理
#### プレイヤーページ
- ブランドでスタイル変更なし
- ブラックリスト機能（関連コンテンツに対して）
- 特定のコンテンツをリストに表示させない処理（関連コンテンツに対して）
