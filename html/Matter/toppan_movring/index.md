# 凸版印刷movring版

## 
■DEV JCW  
- https://dev.j-creativeworks.net/toppan_insatsu/202111/movringPlayer/aws/
- https://dev.j-creativeworks.net/toppan_insatsu/202111/movringPlayer/jcw/html/
- movrng / player

■Git  
- https://bitbucket.org/jcwsystem/toppan-movring/src/master/

■Box  
- https://j-creativeworks.box.com/s/43ci4wjhjd5wfw7oudxzji84ney6dfod

■local  
- C:\Users\t.kunishima\Desktop\HOMEWORKS\t_凸版印刷\202105_movringGA連携

### 仕様
- EQプレイヤーをオブジェクト展開
- GA連携(GA4)「参考：https://semlabo.com/seo/blog/google_analytics4/」
- 送信パラメータ
  - landing	プレイヤー展開時
  - firstplay	初回再生時、再々生時は記録なし、リロード後はあり
  - play	再生時、再再生時（イベント発生のため、ボタン操作とは異なります）
  - pause	一時停止時（イベント発生のため、ボタン操作とは異なります）
  - complete	再生完了時、再々生時は記録あり、リロード後はあり
  - 5秒単位、0/5/10・・・の地点通過時のみ、シーク時スキップは対象外
- indexに、iframeでplayerを呼び出し


### 作業環境
- webpack使用
  - css-loader使用
- JS
  - eqプレイヤーのイベント取得処理
  - documentReady, loadscript使用
- Git


------

