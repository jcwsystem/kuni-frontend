jQuery(function($){
	var host = location.href;
	var qry = $.url(host).param();


	if ($("a.enquete").size() > 0) {
		var href = $("a.enquete").attr("href");
		var sep = "?";
		if (href.indexOf("?") != -1) {
			if (href.split("?")[1] == "") sep = "";
			else sep = "&";
		}
		if (qry.p) {
			href += sep + "c=" + encodeURIComponent(qry.p);
		}
		$("a.enquete").attr("href", href);
	}


	if ($("a.dvr").size() > 0) {
		var href = $("a.dvr").attr("href");
		var sep = "?";
		if (href.indexOf("?") != -1) {
			if (href.split("?")[1] == "") sep = "";
			else sep = "&";
		}
		if (qry.p) {
			href += sep + "c=" + encodeURIComponent(qry.p);
		}
		$("a.dvr").attr("href", href);
	}


	if ($("#player").size() > 0) {
		var src = $("#player").attr("src");
		if (qry.p) {
			src = src.replace(/###set-qry###/g, encodeURIComponent("?c=" + qry.p));
		} else {
			src = src.replace(/###set-qry###/g, "");
		}
		$("#player").attr("src", src);
	}

	if ($("#bbs").size() > 0) {
		var src = $("#bbs").attr("src");
		var sep = "?";
		if (src.indexOf("?") != -1) {
			if (src.split("?")[1] == "") sep = "";
			else sep = "&";
		}
		if (qry.p) {
			src += sep + "p=" + encodeURIComponent(qry.p);
		}
		$("#bbs").attr("src", src);
	}

	if ($("form[id='form'] input[name='a0500']").size() > 0) {
		if (qry.p) {
			$("form[id='form'] input[name='a0500']").val(encodeURIComponent(qry.p));
		}
	}

	if ($("#enq").size() > 0) {
		var src = $("#enq").attr("src");
		if (qry.p) {
			src = src.replace(/###set-qry###/g, encodeURIComponent("?c=" + qry.p));
		} else {
			src = src.replace(/###set-qry###/g, "");
		}
		$("#enq").attr("src", src);
	}

	// ページ遷移ボタンにURLに付いているパラメータを付与
	var paramBtnPage = location.search;
	if ($(".btnPageLowLiveMove a").size() > 0) {
		var href_pageLowLiveMove = $(".btnPageLowLiveMove a").attr("href");
		href_pageLowLiveMove = href_pageLowLiveMove + paramBtnPage;
		$(".btnPageLowLiveMove a").attr("href", href_pageLowLiveMove);
	}
	if ($(".btnPageHighLiveMove a").size() > 0) {
		var href_pageHighLiveMove = $(".btnPageHighLiveMove a").attr("href");
		href_pageHighLiveMove = href_pageHighLiveMove + paramBtnPage;
		$(".btnPageHighLiveMove a").attr("href", href_pageHighLiveMove);
	}
	if ($(".btnPageLowDvrMove a").size() > 0) {
		var href_pageLowDvrMove = $(".btnPageLowDvrMove a").attr("href");
		href_pageLowDvrMove = href_pageLowDvrMove + paramBtnPage;
		$(".btnPageLowDvrMove a").attr("href", href_pageLowDvrMove);
	}
	if ($(".btnPageHighDvrMove a").size() > 0) {
		var href_pageHighDvrMove = $(".btnPageHighDvrMove a").attr("href");
		href_pageHighDvrMove = href_pageHighDvrMove + paramBtnPage;
		$(".btnPageHighDvrMove a").attr("href", href_pageHighDvrMove);
	}

});