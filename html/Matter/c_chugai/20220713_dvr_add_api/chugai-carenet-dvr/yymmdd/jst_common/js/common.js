/*
	common.js
	Ver.1.0.0 2011/07/01
*/
//ビットレートスイッチャー
$(function(){
	//初期設定
	$obj = $('#viewer'), $btn = $('.btn_bitrate').find('a'), h = 'viewer_h.html', l = 'viewer_l.html';
	//イベントリスナー
	$btn.toggle(function(){
		switchBitrate(l);
	},function(){
		switchBitrate(h);
	});
	$obj.load(function(){
		var url = window.viewer.location.href;
		if(url.indexOf(h)>0){
			switchText('低画質（100kbps）版で視聴する');
		}else if(url.indexOf(l)>0){
			switchText('高画質（300kbps）版で視聴する');
		};
	});
	//メソッド
	function switchBitrate(url){
		$obj.attr({src:url});
	}
	function switchText(txt){
		$btn.text(txt);
	}
});
