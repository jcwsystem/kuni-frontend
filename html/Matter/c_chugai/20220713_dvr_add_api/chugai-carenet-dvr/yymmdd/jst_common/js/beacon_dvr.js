﻿//--baseurlにディレクトリ名、baseurl_jstに講演会日を入力--//
//
//
var baseurl,baseurl_jst,bplid,bpeid,img,img_jst,beacon_qry,liveid;
$(function(){
	beacon_qry = $.url(location.href).param();
    baseurl = 'https://ssl-cache.stream.ne.jp/web/live/carenet/live/★yymmdd〇〇〇★/img/chugai_dvr.gif';
    baseurl_jst = 'https://ssl-cache.stream.ne.jp/web/live/ping/carenet/★yymmdd★/chugai_dvr.gif';
	img = document.createElement('img');
	img_jst = document.createElement('img');
	img.setAttribute('id', 'bcimg');
	img_jst.setAttribute('id', 'bcimg_jst');
	liveid = getCookie("liveid");
	setBeaconImg();
});
function setBeaconImg() {
	if(typeof beacon_qry.p === "undefined"){
		beacon_qry.p = '';
	}
	var rn = Math.random().toString(36).slice(-8);
	var src = baseurl + '?p=' + beacon_qry.p + '&rn=' + rn;
	var src_jst = baseurl_jst + '?p=' + beacon_qry.p + '&rn=' + rn;
	
	img.setAttribute('src', src);
	img_jst.setAttribute('src', src_jst);

	setTimeout("setBeaconImg()",60000);
}
function getCookie(c){
	var result = null;

	var cname = c + '=';
	var allcookies = document.cookie;

	var position = allcookies.indexOf(cname);
	if( position != -1 ){
		var st = position + cname.length;
		var ed = allcookies.indexOf( ';', st );
		if( ed == -1 ){
			ed = allcookies.length;
		}
		result = decodeURIComponent(allcookies.substring(st, ed));
	}

	return result;
}