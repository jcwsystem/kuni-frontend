$(function(){
	var paramSet = function() {
		var liveid = paramGetQuerystring("p");
		var query_arr = [];
		/* ----- Cookie ----- */
		if (liveid != "") {
			paramSetCookie('liveid',liveid);
			query_arr.push('liveid='+encodeURIComponent(liveid));
		}

		/* ----- QueryString ----- */
		var qs = query_arr.join("&");
		$(".paramLink").each(function() {

			if ($(this).attr("src")) {
				var param_src = $(this).attr("src");
				param_src = param_src + '?' + qs;
				$(this).attr("src", param_src);
			}

			if ($(this).attr("href")) {
				var param_href = $(this).attr("href");
				param_href = param_href + '?' + qs;
				$(this).attr("href", param_href);
			}
		});
	}

	var paramSetCookie = function(c,v){
		var s = c + "=" + v;
		var p = ";path="+location.pathname;

		document.cookie=s;
	}

	var paramGetQuerystring = function(key, default_){
		if (default_==null) default_="";
		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
		var qs = regex.exec(window.location.href);
		if(qs == null){
			return default_;
		} else {
			return qs[1];
		}
	}
	paramSet();
});