$(function(){
	var co = getCookie();
	if (!("e" in co)) return;

	if ($("a.enquete").size() > 0) {
		var href = $("a.enquete").attr("href");
		var sep = "?";
		if (href.indexOf("?") != -1) {
			if (href.split("?")[1] == "") sep = "";
			else sep = "&";
		}
		
		href += sep + "c=" + encodeURIComponent(co.e);
		$("a.enquete").attr("href", href);
	}

	if ($("form[id='form'] input[name='a0500']").size() > 0) {
		$("form[id='form'] input[name='a0500']").val(encodeURIComponent(co.e));
	}

/**
 * cookie情報を取得する
 * cookieの値はURLデコードされ、特殊文字はサニタイジングします。
 *
 * @return cookieの値を連想配列化したObjectのインスタンス
 */
function getCookie()
{
	var result = {};
	
	var allcookie = document.cookie;
	if(allcookie && allcookie.length > 0){
		var cookies = allcookie.split(";");
		for(var i = 0; i < cookies.length; i++){
			var c = cookies[i].split("=");
			if(c.length >= 2)result[c[0].replace(/(^\s+)|(\s+$)/g, "")] = escapeString(decodeURIComponent(c[1]));
		}
	}
	return result;
}
/**
 * 特殊文字をエスケープします。
 * エスケープする文字
 * & => &amp;
 * " => &quot;
 * ' => &#039;
 * < => &lt;
 * > => &gt;
 *
 * @param str エスケープ対象の文字列
 *
 * @return エスケープ後の文字列
 */
function escapeString(str)
{
	var s = str;
	return s.replace(/[&"'<>]/g, function(m){return"&"+["amp","quot","#039","lt","gt"]["&\"'<>".indexOf(m)]+";"});
}
});