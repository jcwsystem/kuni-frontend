$(function(){
	var host = location.href;
	var qry = $.url(host).param();


	if ($("a.enquete").size() > 0) {
		var href = $("a.enquete").attr("href");
		var sep = "?";
		if (href.indexOf("?") != -1) {
			if (href.split("?")[1] == "") sep = "";
			else sep = "&";
		}
		
		if (qry.c) {
			href += sep + "c=" + (qry.c);
		}
		$("a.enquete").attr("href", href);
	}

});
