(function() {
	var LOG_API = 'https://test01.co3.co.jp/chugai/api/set_dvrlog_cn.html';
	var LOG_PARAM = $.url(location.href).param(); // purl.js 使用

	function Logging() {
		this.iframe = null;
		this.isSendedFirstPlay = false;
		this.prevViewTime = null;
		this.params = {
			caslogid: null,
			seminarid: null,
			seminartitle: null
		};

		var dom = document.getElementsByClassName('main-video-content');
		if (!dom || dom.length <= 0) {
			return;
		}

		var iframe = dom[0].getElementsByTagName('iframe');
		if (!iframe || iframe.length <= 0) {
			return;
		}

		this.iframe = iframe[0];

		// param
		if(typeof LOG_PARAM.uid === "undefined"){
			this.params.caslogid = '';
		} else {
			this.params.caslogid = LOG_PARAM.uid;
		}
		if (typeof LOG_PARAM.liveid === 'undefined'){
			this.params.seminarid = '';
		} else {
			this.params.seminarid = LOG_PARAM.liveid;
		}
		// seminartitle
		this.params.seminartitle = encodeURIComponent(pl_title);
	}
	Logging.prototype.start = function() {
		if (!this.iframe) {
			return;
		}

		var self = this;
		var iframe = this.iframe;

		window.addEventListener('message', function(event) {
			if (checkOrigin(event.origin, iframe.src)){
				if (typeof event.data !== 'string') {
					return;
				}

				var eventData = event.data.split("|");
				if (eventData.length <= 1) {
					return;
				}

				var eventName = eventData[0];
				var currentTime = Math.floor(parseInt(eventData[1], 10) / 30) / 2;

				switch(eventName) {
					case 'play':
						if (!self.isSendedFirstPlay) {
							self.send('firstplay', currentTime);
							self.isSendedFirstPlay = true;
						}
						self.send(eventName, currentTime);
						break;

					case 'pause':
						self.send(eventName, currentTime);
						break;

					case 'seeked':
						self.send(eventName, currentTime);
						break;

					case 'ended':
						self.send('complete', currentTime);
						break;

					case 'timeupdate':
						if (self.prevViewTime === null || Math.abs(currentTime - self.prevViewTime) >= 0.5) {
							self.send(eventName, currentTime);
							self.prevViewTime = currentTime;
						}
						break;
				}
			}
		});
	};
	Logging.prototype.send = function(event, pos) {
		var query = [];
		for(key in this.params) {
			if (key !== 'seminartitle') {
				query.push(key + '=' + this.params[key]);
			} else {
				query.push(key + '=' + encodeURIComponent(this.params[key]));
			}
		}
		query.push('mode=' + pl_mode);
		query.push('event=' + event);
		query.push('pos=' + pos);
		query = query.join('&');
		// console.log(query);

		var xhr = new XMLHttpRequest();
		xhr.open("GET", LOG_API + '?' + query, true);
		xhr.send(null);
	};

	function checkOrigin(eventOrigin, uri) {
		var origin = '';
		var reg = /^(?:([^:\/?#]+):)?(?:\/\/([^\/?#]*))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?/;
		var m = uri.match(reg);

		if (!m) {
			return false;
		}

		origin = m[1] + '://' + m[2];
		if (eventOrigin !== origin) {
			return false;
		}
		return true;
	}

	function domReady(callback){
		function onLoaded() {
			document.removeEventListener('DOMContentLoaded', onLoaded);
			window.removeEventListener('load', onLoaded);
			callback();
		}

		if (document.readyState === 'complete' || (document.readyState !== 'loading' && !document.documentElement.doScroll)) {
			onLoaded();
		} else {
			document.addEventListener('DOMContentLoaded', onLoaded);
			window.addEventListener('load', onLoaded);
		}
	};

	function init() {
		var log = new Logging();
		log.start();
	}

	domReady(init);
})();