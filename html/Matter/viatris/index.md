# ヴィアトリス（松井）：2021年10月

## 
■DEV JCW  
- https://dev.j-creativeworks.net/viatris-live-no-member/web/live/viatris2021/1104/index.html
- viatris / live

■Git  
- https://bitbucket.org/jcwsystem/individual-project/src/master/

■local  
- C:\Users\t.kunishima\Desktop\HOMEWORKS\v_ヴィアトリス製薬\202110_ライブページ制作（松井）

### 仕様
- 疑似認証
- 時限設定あり（ポーリングなし。アイパス入力の際にサーバー時間をクリック時に時限判定）
- BBSアンケートあり
- livelog（livelog.js）取得なし
- トルティーアアンケートを入室ログとして使用。（名前を入力させて、それをログとして使用）
- プレイヤーはiframe展開
- プレイヤーページは時限設定による切替なし、リダイレクトなし。


### 作業環境
- webpack使用
- JS
  - 設定はconfig.jsに記載。xmlなし（オープン・クローズの時間は、絶対にxmlで書いたほうがよい！！）
  - ログイン処理
    - 時限設定（ポーリングなし）
  - BBSのパラメータ付与は、独自でやったので参考に今後使用しないほうがよい。（本来ならform_qryid.jsを使うと便利！）

### 参考に出来そうなもの
- 入室ログとして使用するために改造したトルティーアアンケート
- ポーリングしない時限設定。クリック時に時限判定

### 振り返り
- 時限設定したときにxmlを使わなかったので、テストしづらいものになってしまった。
- ディレクトリ構成あまい。webpackはルートのみで使えるようにできたら良かった。
------

