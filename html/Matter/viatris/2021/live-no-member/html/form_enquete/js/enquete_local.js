/* --------------------------------------------------


	TORTILLA ENQUETE ver 1.0


-------------------------------------------------- */
var TORTILLA_ENQUETE = function() {
	var textList = {
		sended:		"回答が送信されました。",
		answered:	"このアンケートは回答済みです。",
		noAnswer:	"回答がされていません。",
		error:		"エラーが発生しました。",
		required:	"必須"
	};

	var limit = {
		text: 180,
		op1: 100,
		op2: 50,
		op3: 25,
		op4: 25,
	};

	var param = parseUri(location.href).queryKey;

	var mode = {
		dev: location.href.indexOf(".dev") != -1 || location.href.indexOf("dev.") != -1 || location.href.indexOf("localhost") != -1,
		stg: location.href.indexOf(".dev") != -1 || location.href.indexOf("/dev/") != -1
	};

	var key = $.cookie('key');

	if (!key) {
		key = isThis.strings + "_" + randobet(8) + (new Date().getTime() % (100000000));	// プレフィックス2〜3桁＋半角アンダースコア＋ランダム半角英数字8桁＋UNIXタイムms下8桁＝合計19〜20桁
		$.cookie('key', key, {expires: 1, path: '/'});
	}



	/* --------------------------------------------------
		表示制御
	-------------------------------------------------- */
	var displayControl = function() {
		// アンカーリンクスクロール
		function contorlAnchorLinkScroll() {
			$("body").on("click", "a[href^='#']", function() {
				var pos = $($(this).attr("href")).position();
				if (pos) {
					$('html,body').animate({
						scrollTop: pos.top
					}, {
						duration: 200
					});
				}

				return false;
			})
		};

		contorlAnchorLinkScroll();
	};



	/* --------------------------------------------------

		アンケートページ
	
	-------------------------------------------------- */
	var enqueteMain = function() {
		var enqid = param.enqid;

		function makeCloseButton() {
			$("a.btnSubmit").hide();
			$("a.btnSubmit.def").show();
			$("a.btnSubmit.def").html("<span>閉じる</span>");
			$("a.btnSubmit.def").attr("data-submit-type", "close");
		}

		function enquete() {
			var apiParams = {
				enqid: enqid,
				key: key
			};

			function onsuccess(json) {
				function writeAnswered() {
					$("#enquete_form").html("<div class=\"section01 noLine\"><p>" + textList.answered + "</p></div>");
					makeCloseButton();
				}

				var enqueteContent, tmpHtml, required;
				var i, j, attr_id;
				var answered = false;

				$(".loading").removeClass("loading");

				if (json.resultcode == "0103") {
					writeAnswered();
					return;
				}

				for (i = 0; i < json.result.enquete.length; i++) {
					enqueteContent = json.result.enquete[i];

					if (enqueteContent.answered === "on") {
						answered = true;
						break;
					}

					required = (enqueteContent.required === "on") ? " validate[required]" : "";

					tmpHtml = "";
					tmpHtml += "<div class=\"section01\">";

					// タイトル
					tmpHtml += "<h2 class=\"title01\">" + enqueteContent.qtitle + (required ? "<span>" + textList.required + "</span>" : "") + "</h2>";

					// 設問内容
					tmpHtml += "<p class=\"txt01\">" + enqueteContent.qdescription + "</p>";

					// 選択肢
					switch (enqueteContent.atype) {
						case "radio":
							tmpHtml += "<div class=\"enqBox01\">";
							tmpHtml += "<span id=\"" + enqueteContent.qid + "\" class=\"confirm\" style=\"display:none;\"></span>";
							tmpHtml += "<dl class=\"def\">";

							for (j = 0; j < enqueteContent.answer.length; j++) {
								attr_id = "a" + enqueteContent.qid + "-" + (j + 1);
								tmpHtml += "<dt>";
								tmpHtml += "<input type=\"radio\" name=\"" + enqueteContent.qid + "\" id=\"" + attr_id + "\" value=\"" + enqueteContent.answer[j].aid + "\" class=\"def fm" + required + "\">";
								tmpHtml += "</dt>";
								tmpHtml += "<dd>";
								tmpHtml += "<label for=\"" + attr_id + "\" class=\"def\">" + enqueteContent.answer[j].atitle + "</label>";
								tmpHtml += "</dd>";
							}

							tmpHtml += "</dl>";
							tmpHtml += "</div>";

							break;

						case "check":
							tmpHtml += "<div class=\"enqBox01\">";
							tmpHtml += "<span id=\"" + enqueteContent.qid + "\" class=\"confirm\" style=\"display:none;\"></span>";
							tmpHtml += "<dl class=\"def\">";

							for (j = 0; j < enqueteContent.answer.length; j++) {
								attr_id = "a" + enqueteContent.qid + "-" + (j + 1);
								tmpHtml += "<dt>";
								tmpHtml += "<input type=\"checkbox\" name=\"" + enqueteContent.qid + "\" id=\"" + attr_id + "\" value=\"" + enqueteContent.answer[j].aid + "\" class=\"def fm" + required + "\">";
								tmpHtml += "</dt>";
								tmpHtml += "<dd>";
								tmpHtml += "<label for=\"" + attr_id + "\" class=\"def\">" + enqueteContent.answer[j].atitle + "</label>";
								tmpHtml += "</dd>";
							}

							tmpHtml += "</dl>";
							tmpHtml += "</div>";

							break;

						case "drop":
							tmpHtml += "<div class=\"enqBox01 selectBox\">";
							tmpHtml += "<span id=\"" + enqueteContent.qid + "\" class=\"confirm\" style=\"display:none;\"></span>";
							tmpHtml += "<select name=\"" + enqueteContent.qid + "\" id=\"a" + enqueteContent.qid + "\" class=\"def fm" + required + "\">";
							tmpHtml += "<option value=\"\">選択してください</option>";
							for (j = 0; j < enqueteContent.answer.length; j++) {
								tmpHtml += "<option value=\"" + enqueteContent.answer[j].aid + "\">" + enqueteContent.answer[j].atitle + "</option>";
							}
							tmpHtml += "</select>";
							tmpHtml += "</div>";

							break;

						case "textbox":
							tmpHtml += "<div class=\"enqBox01\">";
							tmpHtml += "<span id=\"text_" + enqueteContent.qid + "\" class=\"confirm\" style=\"display:none; word-wrap:break-word;\"></span>";
							tmpHtml += "<input type=\"text\" name=\"text_" + enqueteContent.qid + "\" maxlength=\"" + limit.text + "\" id=\"a" + enqueteContent.qid + "\" class=\"def fm validate[maxSize[" + limit.text + "]]" + required + "\">";
							tmpHtml += "</div>";

							break;

						case "textarea":
							tmpHtml += "<div class=\"enqBox01\">";
							tmpHtml += "<span id=\"text_" + enqueteContent.qid + "\" class=\"confirm\" style=\"display:none; word-wrap:break-word;\"></span>";
							tmpHtml += "<textarea name=\"text_" + enqueteContent.qid + "\" maxlength=\"" + limit.text + "\" id=\"a" + enqueteContent.qid + "\" class=\"def fm validate[maxSize[" + limit.text + "]]" + required + "\"></textarea>";
							tmpHtml += "</div>";

							break;
					}
					
					tmpHtml += "</div>";

					$("#enquete_form").append(tmpHtml);
				}

				if (answered) {
					writeAnswered();
					return;
				}

				$("#enquete_form .section01:last").addClass("noLine");

				if (json.result.conf == "on") {
					$("a.btnSubmit[data-submit-type=\"check\"]").attr("data-confirmation", "on");
				} else {
					$("a.btnSubmit[data-submit-type=\"check\"] span").html(
						$("a.btnSubmit[data-submit-type=\"send\"]").text()
					);
				}

				$("#enquete_form").submit(function() {
					$("a.btnSubmit[data-submit-type=\"check\"]").click();
					return false;
				});
			}

			function onerror(json) {
				$(".loading").removeClass("loading");
				$("#enquete_form").html("<div class=\"section01 noLine\">" + textList.error + "<br>(" + json.rescode + " : " + json.msg + ")</div>");
				makeCloseButton();
			}

			api.get_enquete(apiParams, onsuccess, onerror);
		}

		function confirm() {
			$(".fm").each(function() {
				var id = $(this).attr("id");
				var fm = $("#" + id);

				//フォームのタイプによって処理を分岐
				var val;
				switch (fm.prop("type")) {
					case 'text':
					case 'password':
						val = fm.val();
						val = escapeHtml(val);
						$("span#" + $(this).attr("name")).append(val);
						break;

					case 'select-one':
						$("span#" + $(this).attr("name")).append(fm.find("option:selected").text());
						break;

					case 'radio':
						if (fm.prop('checked')) {
							val = $("label[for='" + id + "']").text();
							$("span#" + $(this).attr("name")).append(val);
						}
						break;

					case 'checkbox':
						if (fm.prop('checked')) {
							val = $("label[for='" + id + "']").text();
							$("span#" + $(this).attr("name")).append(val + "<br>");
						}
						break;

					case 'textarea':
						val = fm.val();
						val = escapeHtml(val);
						val = val.replace(/\n/g, '<br>');
						$("span#" + $(this).attr("name")).append(val);
						break;
				}
			});

			$("span.confirm").each(function() {
				if ($(this).html() === "") {
					$(this).html("(回答なし)");
				}
			});

			// 入力項目を隠し、回答変更・送信ボタンを表示
			$(".def").hide();
			$(".confirm").show();
		}

		function send() {
			var i;
			
			// ボタン無効化
			$("a.btnSubmit").attr("data-submit-type", "disable");
			// 入力した回答を配列へ格納
			var sendParam = $("#enquete_form").serializeArray();
			var ans_arr = {};
			for (i = 0; i < sendParam.length; i++) {
				if (sendParam[i].name.match(/text_/)) {
					// テキストボックス
					if (sendParam[i].value !== "") {
						var qid = sendParam[i].name.replace(/text_/g, "");
						ans_arr[sendParam[i].name] = {
							qid: qid,
							atext: sendParam[i].value
						};
					}
				} else {
					if (sendParam[i].name in ans_arr) {
						ans_arr[sendParam[i].name].aid += "-" + sendParam[i].value;
					} else {
						ans_arr[sendParam[i].name] = {
							qid: sendParam[i].name,
							aid: sendParam[i].value
						};
					}
				}
			}

			// 並列処理用メソッド
			function dfd_set_enqanswer(qid, aid, atext) {
				var i;

				var dfd = $.Deferred();
				var apiParams = {
					enqid: enqid,
					key: key,
					qid: qid,
					aid: aid,
					atext: atext
				};

				for (i in param) {
					if (i.match(/op[1-4]/)) {
						apiParams[i] = param[i].substr(0, limit[i]);
					}
				}
				
				consoleLog(apiParams, mode.dev);

				function onsuccess(json) {
					dfd.resolve(json);
				}

				function onerror(json) {
					dfd.reject(json);
				}
				api.set_enqanswer(apiParams, onsuccess, onerror);
				return dfd.promise();
			}

			var funcArray = [];
			$.each(ans_arr, function(index, val) {
				funcArray.push(dfd_set_enqanswer(val.qid, val.aid, val.atext));
			});

			$.when.apply($, funcArray).done(function(json) {
				// 親ページを更新
				if (window.opener) {
					// window.opener.document.location.reload();
				}

				// 完了画面へ表示
				$("#enquete_form").html("<div class=\"section01 noLine\"><p>" + textList.sended + "</p></div>");
				makeCloseButton();

			}).fail(function(json) {
				// エラーを表示
				$("#enquete_form").html("<div class=\"section01 noLine\">" + textList.error + "<br>(" + json.rescode + " : " + json.msg + ")</div>");
				makeCloseButton();

			});
		}

		function validate() {
			$("#enquete_form").validationEngine('attach', {
				promptPosition: "topRight:0,-10"
			});
			return $("#enquete_form").validationEngine('validate');
		}

		// ボタンイベント
		$("a.btnSubmit").on('click', function() {
			var answerArray, i;
			var noAnswer = true;

			switch ($(this).attr("data-submit-type")) {
				case "disable":
					break;
					
				case "check":
					// 入力チェック
					answerArray = $("#enquete_form").serializeArray();

					for (i = 0; i < answerArray.length; i++) {
						if (answerArray[i].value) {
							noAnswer = false;
						}
					}
					
					if (noAnswer) {
						alert(textList.noAnswer);

					} else {
						if ($(this).attr("data-confirmation") == "on") {
							if (validate()) {
								$('html,body').animate({scrollTop: 0}, 0);
								confirm();
							}
						} else {
							$("a.btnSubmit[data-submit-type=\"send\"]").click();
						}
					}

					break;
					
				case "back":
					// 入力項目を表示し、回答変更・送信ボタンを非表示
					$(".def").show();
					$(".confirm").hide();
					$("span.confirm").empty();
					$('html,body').animate({scrollTop: 0}, 0);
					break;
					
				case "send":
					// 入力チェック
					if (validate()) {
						send();
					} else {
						// 入力項目を表示し、回答変更・送信ボタンを非表示
						$(".def").show();
						$(".confirm").hide();
						$("span.confirm").empty();
						// エラー表示位置を更新
						$("#enquete_form").validationEngine("updatePromptsPosition");
					}
					break;
					
				case "close":
					window.close();
					break;

			}

			return false;
		});

		enquete();
	};

	/* --------------------------------------------------
		その他
	-------------------------------------------------- */
	var escapeHtml = function(str) {
		var escapeMap = {
			'&': '&amp;',
			"'": '&#x27;',
			'`': '&#x60;',
			'"': '&quot;',
			'<': '&lt;',
			'>': '&gt;'
		};

		var escapeReg = '[';
		for (var p in escapeMap) {
			if (escapeMap.hasOwnProperty(p)) {
				escapeReg += p;
			}
		}
		escapeReg += ']';

		var reg;
		reg = new RegExp(escapeReg, 'g');

		str = (str === null || str === undefined) ? '' : '' + str;
		return str.replace(reg, function(match) {
			return escapeMap[match];
		});
	};

	var onsuccess = function(json) {
		console.log(json);
	};

	var onerror = function(data) {
		console.log(data);
	};



	/* --------------------------------------------------
		Main
	-------------------------------------------------- */
	var api = new IFAPI(mode);

	displayControl();
	enqueteMain();
};





/* --------------------------------------------------


	Interface API ver 1.0


-------------------------------------------------- */
var IFAPI = function(mode) {
	var self = this;

	var baseParams = {};

	if (mode.dev) {
	}

	// 設定値
	var protocol = "http://";
	var apiHost = protocol + "localhost/tortilla";
	var apiBasePass = '/users/viatris2/api';
	var apiBaseUri = apiHost + apiBasePass;

	// APIのURL設定
	var API_URLs = {
		get_enquete:	apiBaseUri + "/get_enquete.aspx",
		set_enqanswer:	apiBaseUri + "/set_enqanswer.aspx"
	};

	// リクエスト失敗時メッセージ
	var requestErrorMsg = "AJAX通信エラー";

	// リクエスト処理
	var req = function (url, params, onsuccess, onerror) {
		$.ajax({
			type: "GET",
			url: url,
			data: params,
			cache: false,
			dataType: "jsonp"
		})
		.done(function(json){
			// リザルトコードチェック ///////
			var checkedObj = checkResCode(json);
			if(checkedObj.flag){
				$.isFunction(onsuccess) && onsuccess(json, checkedObj);
			} else {
				$.isFunction(onerror) && onerror(checkedObj);
			}
		})
		.fail(function(xhr, status, error){
			// console.log(xhr);
			var result = {
				flag: false,
				rescode: "",
				msg: requestErrorMsg
			};

			// consoleLog("AJAX Failed");

			$.isFunction(onerror) && onerror(result, xhr, status, error);
		});
	};

	/*===================================================
	 	各インターフェース
	====================================================*/
	// 1 アンケート内容一括応答API
	self.get_enquete = function(params, onsuccess, onerror) {
		var url = API_URLs.get_enquete;
		req(url, $.extend(params, baseParams), onsuccess, onerror);
	};

	// 2 アンケート回答受信API
	self.set_enqanswer = function(params, onsuccess, onerror) {
		var url = API_URLs.set_enqanswer;
		req(url, $.extend(params, baseParams), onsuccess, onerror);
	};

	/*===================================================
	 	リザルトコードチェック
	====================================================*/
	var checkResCode = function(data){
		var result = {
			flag:　false,
			rescode: "",
			msg:　""
		};

		var resultCode = data.resultcode ? data.resultcode : false;

		// 処理結果コード存在チェック
		if(resultCode){
			// OKかチェック
			result.flag = $.inArray(resultCode, okResultCode) != -1;

			// コードに対応したメッセージを格納
			result.rescode = resultCode;
			result.msg = resultText[resultCode];
		} else {
			// 処理結果コードが見つからない場合
			result.msg = "処理結果コードなし";
		}

		return result;
	};

	// リザルトコード対応オブジェクト:API仕様書準拠
	var resultText = {
		"0000": "正常終了",
		"0101": "パラメータエラー",
		"0102": "対象データなし",
		"0103": "回答済み",
		"0199": "システムエラー",
		"0201": "パラメータエラー",
		"0202": "対象データなし",
		"0299": "システムエラー"
	};

	// 処理成功許可リザルトコード(array)
	var okResultCode = [
		"0000",
		"0103"
	];
};





$(TORTILLA_ENQUETE);
