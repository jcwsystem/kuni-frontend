	/**
	 * 時間文字列のフォーマットチェック
	 * YYYY/MM/DD HH:ii:ss (2021/7のブラッシュアップで使用不可)
	 * またはタイムゾーン（Ex: 1987-04-01T+0900）を許可
	 * @param string str 
	 */
class CheckTimeFormat {
  check(str) {
    if (typeof str === 'string' && (str.match(/^(\d{1,4})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])T([0-1][0-9]|2[0-4]):[0-5][0-9]:[0-5][0-9](Z|(\+|-)([01][0-9]|2[0-4]):([0-5][0-9])?)$/))) {
      return true;
    }
    return false;

  }
}
export default new CheckTimeFormat();

