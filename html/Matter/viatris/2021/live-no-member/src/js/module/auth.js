import jsCookie from "js-cookie";
import config from "../config";

class Auth {
	constructor() {
		this.loginCookieName = config.loginCookieName;
		this.userList = config.userList;
	}

	// ログインしてユーザー情報をクッキーへ書き込み
	login(userId, password) {
		if (config.liveId && config.livePass === password) {
			// jsCookie.set(this.loginCookieName, JSON.stringify(userId), {expires: config.loginTerm});
			// 保存期間はsessionで。（期間設定なし）
			jsCookie.set(this.loginCookieName, encodeURIComponent(userId));
			return true;
		} else {
			return false;
		}
	}

	// ログアウト
	logout() {
		jsCookie.remove(this.loginCookieName);
	}

	// ログインしているかどうか
	get status() {
		return jsCookie.get(this.loginCookieName) ? true : false;
	}

	// ログイン済みのuserIdの取得
	get loginedUserId() {
		// return JSON.parse(jsCookie.get(this.loginCookieName));
		return jsCookie.get(this.loginCookieName);
	}
}

export default new Auth();
