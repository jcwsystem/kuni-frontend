import $ from "jquery";
import Auth from "./module/auth";
import config from "./config";
import checkTimeFormat from "./module/checkTimeFormat";

// クッキーリセット
Auth.logout();

$(() => {
  const $errorMsg = $("#info");

  $("#submit").on("click", () => {
    $.when(
      $.ajax({
        type: "GET",
        cache: false,
      }),

      // DOM待ち
      $.ready
    ).then((result) => {
      const [data, status, xhr] = result;

      const serverDate = new Date(xhr.getResponseHeader("Date")),
						openTime = new Date(config.openTime),
						closeTime = new Date(config.closeTime);

      const nowMoment = serverDate.getTime(),
						openMoment = openTime.getTime(),
						closeMoment = closeTime.getTime();

      let check_open = false;

      // 時間フォーマットが正の場合であればcheck_openの判定をする。不正であれば処理せず初期値のfalseのまま
      if (serverDate && checkTimeFormat.check(config.openTime) && checkTimeFormat.check(config.closeTime)) {
        // if (nowMoment < openMoment) {
        //   check_open = false;
        //   // console.log("公開前")
        // }
        // if (nowMoment >= closeMoment) {
        //   check_open = false;
        //   // console.log("公開終了")
        // }
        if (nowMoment > openMoment && nowMoment < closeMoment) {
          check_open = true;
          // console.log("公開中")
        }
      }

      const userId = $("#user_name").val();
      const password = $("#password").val();

      // 入力チェック
      if (userId == "" || password == "") {
        $errorMsg.text("必須項目が未入力です。");
        return false;
      }

      // ログイン認証（クッキーへ書き込み）
      Auth.login(userId, password);

      if (!Auth.status) {
        $errorMsg.text("IDまたはパスワードが間違っています。");
        return false;
      }

      if (!check_open) {
        const setOpenTime = `${openTime.getFullYear()}年${openTime.getMonth() + 1}月${openTime.getDate()}日${openTime.getHours()}時${openTime.getMinutes()}分`;

				if (nowMoment >= closeMoment) {
          $errorMsg.text(`ライブ配信は終了しました。`);
          return false;
        }

        $errorMsg.text(`開始時間は${setOpenTime}からになります。それまでお待ち下さい。`);
        return false;
      }

      // ページ遷移
      location.href = `${config.page.form}?enqid=${config.enqid}&op1=${userId}`;
      return false;
    });
  });
});
