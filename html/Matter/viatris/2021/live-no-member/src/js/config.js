const config = {
	openTime: "2021-10-20T11:35:00+09:00",
	closeTime: "2023-10-31T11:36:00+09:00",

	liveId: "live1104",
	livePass: "Viatris1104",

	loginCookieName: "liveId",
	// loginTerm: 3,

	enqid: 3,

	page: {
		login: "index.html",
		form: "form_enquete/index.html",
		live: "live/index.html",
	},

	// logWorks: {
	// 	cid: "9",
	// 	liveid: "",
	// }
};

export default config;
