var xmlHttp = createHttpRequest();
var logid = "0";
var interval = 10000;     //ログ取得間隔（初回用）　２回目以降はweb.configの設定による
var timer;                //タイマー用

if (xmlHttp != null) {
  callLiveLog();
  timer = window.setInterval('callLiveLog()', interval);
}

//XMLHttpRequestオブジェクト生成
function createHttpRequest() {
  //Win ie用
  if (window.ActiveXObject) {
    try {
      //MSXML2以降用
      return new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        //旧MSXML用
        return new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e2) {
        return null;
      }
    }
  } else if (window.XMLHttpRequest) {
    //Win ie以外のXMLHttpRequestオブジェクト実装ブラウザ用
    return new XMLHttpRequest();
  } else {
    return null;
  }
}

//ライブログ記録処理呼び出し
function callLiveLog() {
  if (xmlHttp != null) {
  	xmlHttp.open("GET", livelogpath + "livelog.aspx?id=" + logid + "&p=" + livelogpage + "&t=" + (new Date()).getTime(), true);
    xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
        var result = xmlHttp.responseText;  //レスポンス結果(logid,タイマー間隔)
        if (result.indexOf(",") != -1) {
          var tmp = result.split(",");
          if (!isNaN(tmp[0])) {
            logid = tmp[0];
          }
          if (logid == -1) {
            //ログ取得終了
            clearInterval(timer);
          }
          else if (!isNaN(tmp[1]) && interval != parseInt(tmp[1])) {
            //タイマー間隔が変更されたので再設定
            interval = parseInt(tmp[1]);
            clearInterval(timer);
            timer = window.setInterval('callLiveLog()', interval);
          }
        }
      }
    }
    xmlHttp.send(null);
  }
}
