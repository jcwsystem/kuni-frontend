/* ----------------------------------------
	Console Log
---------------------------------------- */
window.consoleLog = function(val, mode) {
	if (mode) {
		try {
			console.log(val);
		} catch(e) {};
	}
};





/* ----------------------------------------
	ランダムな半角英数字の文字列を生成
---------------------------------------- */
var randobet = function(n, b) {
	b = b || '';
	var a = 'abcdefghijklmnopqrstuvwxyz'
		+ 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		+ '0123456789'
		+ b;
	a = a.split('');
	var s = '';
	for (var i = 0; i < n; i++) {
		s += a[Math.floor(Math.random() * a.length)];
	}
	return s;
};
// randobet(10, '-_!'); // 2OM_g0-76n





/* ----------------------------------------
	UA判定
---------------------------------------- */
var isThis = new function() {
	this.ua = navigator.userAgent;

	this.iPhone = this.ua.indexOf("iPhone") != -1;
	this.iPad = this.ua.indexOf("iPad") != -1;
	this.iOS = this.iPhone || this.iPad;

	this.Android = this.ua.indexOf("Android") != -1;
	this.Android2 = this.ua.indexOf("Android 2") != -1;
	this.Android4 = this.ua.indexOf("Android 4") != -1;
	this.AndroidSmartPhone = this.Android && this.ua.indexOf("Mobile") != -1 && this.ua.indexOf("A1_07") == -1 && this.ua.indexOf("SC-01C") == -1;
	this.AndroidTablet = this.Android && (this.ua.indexOf("Mobile") == -1 || this.ua.indexOf("A1_07") != -1 || this.ua.indexOf("SC-01C") != -1);

	this.sp = this.iPhone || this.AndroidSmartPhone;
	this.tablet = this.iPad || this.AndroidTablet;

	this.ED = this.ua.indexOf("Edge") != -1;
	this.IE = this.ua.indexOf("MSIE") != -1 || this.ua.indexOf("Trident") != -1;
	this.FF = this.ua.indexOf("Firefox") != -1;
	this.CH = this.ua.indexOf("Chrome") != -1;
	this.SF = this.ua.indexOf("Safari") != -1;

	this.strings = this.iPhone ? "IPH" : (
		this.iPad ? "IPD" : (
			this.AndroidSmartPhone ? "ANS" : (this.AndroidTablet ? "ANT" : null)
		)
	);

	if (!this.strings) {
		this.strings = this.ED ? "ED" : (
			this.IE ? "IE" : (
				this.FF ? "FF" :  (
					this.CH ? "CH" : (this.SF ? "SF" : "")
				)
			)
		);
	}
};





/* ----------------------------------------
	URL Parse
---------------------------------------- */
// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

/* 使い方
var param = parseUri(location.href).queryKey;
でparamにURLのクエリーの値が返ってきます
*/

function parseUri (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};
