var Api = function() {
	//---------------------
	// cosnt
	//---------------------
	var self = this;

	var replaceDirName = "###DIR_NAME###";
	var url = "https://tortilla.co3.co.jp/users/" + replaceDirName + "/api/get_enqresult.aspx";

	// リザルトコード
	self.resultCode = {
		success: "0000",
		paramError: "0101",
		noData: "0102",
		systemError: "0199"
	};

	//---------------------
	// public
	//---------------------
	// アンケート結果json取得
	self.getEnqResult = function(dirName, vqid, option) {
		var d = new $.Deferred();

		$.ajax({
			type: "GET",
			url: url.replace(replaceDirName, dirName),
			dataType: "jsonp",
			data: {
				vqid: vqid,
			},
			context: option,
			cache: false,
			timeout: 3000
		}).then(function(json) {

			// 回答結果が順不同で来るのでソート
			if (json.result && json.result.length > 0) {
				json.result.sort(function(a, b) {
					return a.vaid < b.vaid ? -1 : 1;
				})
			}

			d.resolve({enquete: json, option: this});
		}, function(xhr, status, error) {
			console.error('AJAX通信エラー(JSON)', xhr, status, error);
			d.reject();

		})

		return d.promise();
	};

	//---------------------
	// private
	//---------------------




};