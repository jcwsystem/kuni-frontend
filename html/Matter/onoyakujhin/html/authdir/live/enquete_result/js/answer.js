$(function() {
  let api = new Api();

  // エスケープ
	var escapeHtml = function(string) {
		if (typeof string !== 'string') {
			return string;
		}
		return string.replace(/[&'`"<>]/g, function (match) {
			return {
				'&': '&amp;',
				"'": '&#x27;',
				'`': '&#x60;',
				'"': '&quot;',
				'<': '&lt;',
				'>': '&gt;',
			}[match]
		});
	};

  $.ajax({
    type: "GET",
    cache: false,
    dataType: 'xml',
    url: "./xml/config.xml",
  }).then(function(data) {

    // コモンオプション
    let $commonOption = $(data).find('commonOption');
    let enqOption = {
      dir:        $commonOption.attr('dir'),
      graf_color: $commonOption.attr('graf_color').split(','),
      count_type: $commonOption.attr('count_type') && $commonOption.attr('count_type') === 'number' ? 'number' : 'percentage',
    }

    // アンケート結果のデータの配列
    let EnqResultList = [];

    // アンケート結果取得の順番を制御
    $.each($(data).find('enqueteGroup enquete'), function(_, enqData) {
      let $enqDataOpition = $(enqData).find('option');

      // 個別のオプションデータをセット
      let individualOption = {
        enq_id:     $enqDataOpition.attr('enq_id'),
        display:    $enqDataOpition.attr('display'),
        wait_text:  $(enqData).find('wait_text').text(),
      };
      $.extend(individualOption, enqOption)

      EnqResultList.push(
        api.getEnqResult(enqOption.dir, individualOption.enq_id, individualOption)
      );

    });


    // アンケート表示処理
    $.when.apply($, EnqResultList).then(function() {

      // 結果の表示タイプ
      let RESULT_DISPLAY = {
        NONE:  '0',
        CLOSE: '1',
        OPEN:  '2',
      }

      // html作成開始
      let $wrapper = $('<div class="wrapper">');

      let html = [];
      $.each(arguments, function(_, enqData) {
        let enquete = enqData.enquete;
        let option = enqData.option;

        // エラーの場合
        if (enquete.resultcode !== api.resultCode.success) {
          console.error("アンケート結果取得に失敗しました。enq_id => " + option.enq_id)
          return false;
        }

        html.push(createHtml(enquete, option, RESULT_DISPLAY));
      });

      $("#answer").append($wrapper.append(html))

      $('.loading').fadeOut(400, function(e) {
        $('.loading').remove();
      })

    })


  });


  function createHtml(enquete, option, resultDisplay) {

    // タイトル
    let description = enquete.description ? enquete.description : null ;
    let $title = '<p class="title">'+ escapeHtml(description)+'</p>';

    // htmlテンプレート
    let html;

    // configの表示設定で、切り替え
    switch(option.display) {
      case resultDisplay.NONE:
        break;

      case resultDisplay.CLOSE:
        html = closeEnquete(option, $title)
        break;

      case resultDisplay.OPEN:
        html = openEnquete(enquete, option, $title);

        break;
    }

    return html;
  }

  function closeEnquete(option, title) {
    let result = [
      '<div class="container">',
        title,
        '<div class="wait_text"><p>'+escapeHtml(option.wait_text)+'</p></div>',
      '</div>'
    ].join("");

    return result;
  }

  function openEnquete(enquete, option, title) {
    let enqueteData = enquete.result;
    let totalCount = 0;

    $.each(enqueteData, function(_, item) {
      totalCount += Number(item.cnt);
    });


    let enqueteItem = function() {
      let htmlTemplate = [];

      $.each(enqueteData, function(index, enq) {
        let enqNumber = escapeHtml(enq.vaid.doubleDigits());
        let barwidth = totalCount === 0 ? '0.0' : (Math.round(enq.cnt / totalCount * 100 * 10) / 10).toFixed(1);

        // カウント数
        let value = function() {
          let val;
          let html;

          // パーセント表示と実数表示の切り替え
          switch(option.count_type) {
            case 'number':
              val = enq.cnt;
              html = '<span>'+ escapeHtml(val) +'</span>件'
              break;
            case 'percentage':
              val = totalCount === 0 ? '0.0' : (Math.round(enq.cnt / totalCount * 100 * 10) / 10).toFixed(1);
              val = val === '100.0' ? '100' : val;
              html = '<span>'+ escapeHtml(val) +'</span>%'
              break;
          }
          return html;
        }

        // style
        let randomColor = "#";
        for(let i = 0; i < 6; i++) {
            randomColor += (16*Math.random() | 0).toString(16);
        }

        let color = option.graf_color[index] ? option.graf_color[index]: randomColor;
        let style_value = color ? 'style="color:'+ escapeHtml(color) +';"': '';
        let style_number = color ? 'style="background-color:'+ escapeHtml(color) +';"': '';
        let style_bar = 'style="background-color:'+ escapeHtml(color) +';width:'+ escapeHtml(barwidth) +'%;"';

        // アンケート結果のリスト作成
        htmlTemplate.push([
          '<li>',
            '<div class="number" '+ style_number +'>'+ enqNumber +'</div>',
            '<div class="choice"><p>'+ enq.answer +'</p></div>',
            '<div class="bar"><span '+ style_bar +'></span></div>',
            '<div class="value"><p '+ style_value +'>'+ value() +'</p></div>',
          '</li>'
        ].join(""));
      });

      // アンケート結果セット
      let result = [
        '<div class="container">',
          title,
          '<ol class="list">',
            htmlTemplate.join(""),
          '</ol>',
        '</div>'
      ].join("")

      return result;
    }
    return enqueteItem();
  }


  // 二桁変換
  Number.prototype.doubleDigits = function () {
    let val = this;
    return val < 10 ? "0" + val : val.toString();
  };
  String.prototype.doubleDigits = function () {
    return parseInt(this, 10).doubleDigits();
  };
});