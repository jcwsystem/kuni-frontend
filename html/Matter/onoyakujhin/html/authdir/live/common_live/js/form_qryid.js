$(function() {
	const qry_user_name = getCookie("user_name");
	const qry_affiliation = getCookie("affiliation");
	const qry_pref_name = getCookie("pref_name");

	const qry_playerid =  paramGetQuerystring("p");
	// const qry_addparam =  paramGetQuerystring("p");

	if (qry_playerid) {
		$("input[name='hidden']").val(qry_playerid);
	}

	//$("input[name='a0100']").val(qry_user_name);
	//$("input[name='a0200']").val(qry_affiliation);
	//$("input[name='a0400']").val(qry_pref_name);
	// $("input[name='a0500']").val(qry_playerid);
});

function getCookie(c){
	var result = null;

	var cname = c + '=';
	var allcookies = document.cookie;

	var position = allcookies.indexOf(cname);
	if( position != -1 ){
		var st = position + cname.length;
		var ed = allcookies.indexOf( ';', st );
		if( ed == -1 ){
			ed = allcookies.length;
		}
		result = decodeURIComponent(allcookies.substring(st, ed));
	}

	return result;
}
function paramGetQuerystring (key, default_){
	if (default_==null) default_="";
	key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
	var qs = regex.exec(window.location.href);
	if(qs == null){
		return default_;
	} else {
		return qs[1];
	}
}