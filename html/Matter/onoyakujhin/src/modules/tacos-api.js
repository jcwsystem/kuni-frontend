import $ from "jquery";

class TacosApi {
	constructor() {
		this.apiBaseUrl = 'https://tacos.co3.co.jp/users/reason_y/api2/';

		this.json;

		// リザルトコード（配列）
		this.resultCode = {
			ok: ['0000']
		};

		// デバッグモード
		this.mode = location.href.indexOf(".dev") != -1 || location.href.indexOf("dev.") != -1 || location.href.indexOf("localhost") != -1;
		this.debugMode = this.mode;
	}

	get loginUser() {
		return this.json['checkkey1'] ? this.json['checkkey1'] : null;
	}

	request(url, param) {
		const d = new $.Deferred();

		$.ajax({
			type: 'GET',
			url: url,
			data: param,
			cache: false,
			dataType: 'jsonp'
		})
		.done((json) => {
			// var resultCode = data.resultcode ? data.resultcode : (data.response_status ? data.response_status : false);
			if ($.inArray(json.resultCode, this.resultCode.ok)) {
				// OK
				this.json = json;
				d.resolve(json);
			} else {
				// NG
				console.error('AJAX通信エラー(TACOS)', json.resultCode);
				d.reject();
			}
		})
		.fail((xhr, status, error) => {
			console.error('AJAX通信エラー(API)', xhr, status, error);
			d.reject();
		})

		return d.promise();
	}

	getApiParam(param = {}) {
		const debugModeParam = this.debugMode ? { uid: 0 } : {};
		return Object.assign(param, debugModeParam);
	}

	getUserInfo() {
		const url = this.apiBaseUrl + 'get_userinfo.aspx';
		return this.request(url, this.getApiParam());
	}

	// setPlayerlog(param) {
	// 	var baseParam = this.getApiParam();
	// 	var newParam = Object.assign(param, baseParam);

	// 	const url = this.apiBaseUrl + 'set_playerlog.aspx';
	// 	return this.request(url, newParam);
	// }
}

export default new TacosApi();