import $ from "jquery";
import Config from "../config";

class BbsApi {
	constructor() {
		this.apiBaseUrl = 'https://d1ox3279py5nvx.cloudfront.net/api/post/';

		this.json;

		// リザルトコード（配列）
		this.resultCode = {
			ok: ['200'],
			ng: ['500']
		};

		// デバッグモード
		this.mode = location.href.indexOf(".dev") != -1 || location.href.indexOf("dev.") != -1 || location.href.indexOf("localhost") != -1;
		this.debugMode = this.mode;
	}

	// get loginUser() {
	// 	return this.json['checkkey1'] ? this.json['checkkey1'] : null;
	// }

	request(url, param, type="GET") {
		const d = new $.Deferred();
		// console.log("----- request -----")
		// console.log("url", url)
		// console.log("param", param)
		// console.log("param", type)

		$.ajax({
			type: type,
			url: url,
			data: param,
			cache: false,
			dataType: 'json'
		})
		.done((json) => {
			if ($.inArray(json.resultCode, this.resultCode.ok)) {
				// OK
				this.json = json;
				d.resolve(json);
			} else {
				// NG
				console.error('AJAX通信エラー(TACOS)', json.resultCode);
				d.reject();
			}
		})
		.fail((xhr, status, error) => {
			console.error('AJAX通信エラー(API)', xhr, status, error);
			d.reject();
		})

		return d.promise();
	}

	getApiParam(param = {}) {
		// const debugModeParam = this.debugMode ? { uid: 0 } : {};
		// return Object.assign(param, debugModeParam);
		// console.log("----- getApiParam -----", Object.assign(param, {bbs_id: 1}))
		return Object.assign(param, {bbs_id: Config.BBSID});
	}

	getBbsPosts(param = {}) {
		const url = this.apiBaseUrl + 'list';
		// const url = '../json/_test.json';
		return this.request(url, this.getApiParam(param), "GET");
	}

	setGoodPosts(param = {}) {
		const url = this.apiBaseUrl + 'likes';
		// const url = '../json/_test.json';
		// console.log("----- setGoodPosts -----", param)
		return this.request(url, this.getApiParam(param), "POST");
	}

}

export default new BbsApi();