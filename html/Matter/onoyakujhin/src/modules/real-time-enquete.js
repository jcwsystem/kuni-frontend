/* --------------------------------------------------
	リアルタイムアンケート表示処理
-------------------------------------------------- */
import $ from "jquery";

export class RealTimeEnquete {
	constructor(targetData, targetDir) {
		this.targetName = targetData;
		this.dir = targetDir;
		this.wrapper = $(`[data-${this.targetName}="wrapper"]`);
		this.content = $(`[data-${this.targetName}="content"]`);
		this.iframe = $(`[data-${this.targetName}="iframe"]`);
		this.openButton = $(`[data-${this.targetName}="open"]`);
		this.closeButton = $(`[data-${this.targetName}="close"]`);

		this.init();
	}

	init() {
		this.clickEvent();
	}

	// アンケート呼び出し（iframe作成）
	create() {
		const $html = `
			<div class="enquete_result_overlay" data-${this.targetName}="close"></div>
			<div class="enquete_result_content" data-${this.targetName}="content">
				<iframe src="${this.dir}" frameborder="0" data-${this.targetName}="iframe"></iframe>
				<div class="enquete_result_closeButton" data-${this.targetName}="close" id="hoge"></div>
			</div>
		`;
		this.wrapper.html($html);
	}

	// アンケートオープン
	clickEvent() {
		this.openButton.on('click', (e) => {
			e.preventDefault();
			this.wrapper.removeClass("is-inactive");
			this.create();

			// 要素の取得し直し
			this.content = $(`[data-${this.targetName}="content"]`);
			this.iframe = $(`[data-${this.targetName}="iframe"]`);
			this.closeButton = $(`[data-${this.targetName}="close"]`);

			$('html').addClass('enqueteResult-open')
			this.closeButton.on('click', (e) => {
				e.preventDefault();
				this.close();
				$('html').removeClass('enqueteResult-open')

			});
		});

	}
	// アンケートクローズ（iframe削除）
	close() {
		const $iframe = $(`[data-${this.targetName}="iframe"]`);

		this.wrapper.addClass("is-inactive")
		$iframe.remove()
	}
};