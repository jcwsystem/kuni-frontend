import $ from "jquery";
import qs from "querystringify";
import Url from "url-parse";

/* ----------------------------------------
	初期設定
---------------------------------------- */

// 公開モード 公開前
const DISP_MODE_BEFORE = 1;
// 公開モード 公開中
const DISP_MODE_OPEN = 2;
// 公開モード 公開終了(メイン視聴終了)
const DISP_MODE_DVR_CLOSE = 3;
// 公開モード 公開終了（DVRのみページ視聴可能の全工程終了）
const DISP_MODE_CLOSE = 4;

// 対象ページ TOP
const TARGET_PAGE_TOP = "TOP";

// 対象ページ SORRY
const TARGET_PAGE_SORRY = "SORRY";

const SETTING_XML_PATH = './common/xml/setting.xml';

// 強制参照モード
const FORCED_BROWSE_KEY = 'forced_mode';

// 強制参照モードの引数を追加セットするリンクに以下classを設定
const FORCED_BROWSE_ADD_CLASS = 'add_forced_mode';


const CHECK_TIMER = 60000;

const param = qs.parse(location.search);


// ****************
// 切り替え用 関数
// ****************
window.switchModel = (function() {
	// ************************************************
	// privateモジュール
	// ************************************************
	/**
	 * 時間チェック（メイン処理）
	 */
	var checkTime = function(target_page) {

		// plyaerページの場合、階層設定
		var xmlDir =  SETTING_XML_PATH;

		return $.when(
		// xml取得
		$.ajax({
			type: 'GET',
			url: xmlDir,
			dataType: 'xml',
			cache: false,
			}),

			// DOM待ち
			$.ready

		).then(function(result) {
			const [xml, status, xhr] = result;

			// XMLヘッダーからサーバーの現在時刻取得
			const currentDate = xhr.getResponseHeader("date");

			// ライブの情報を取得
			const $live_setting = $(xml).find('live_setting');

			if ($live_setting.length) {
				// 対象日のライブの情報を対象
				var open = $live_setting.find("opentime").text();
				var close = $live_setting.find("closetime").text();

				// 公開期間を元に公開ステータス取得
				var openStatus = getOpenStatus(currentDate, open, close);

				// TOPページの場合
				switch(target_page) {
					case TARGET_PAGE_TOP:
						switchTop(openStatus, $live_setting);
						break;
					case TARGET_PAGE_SORRY:
						let txt = $live_setting.find("sorryText").text().trim();
						$('[data-add-text="sorry"]').html(txt);

						switchSorry(openStatus);

						break;
				}

				// 各種リンクに強制閲覧モードセット
				setParmForcedMode();

				return new $.Deferred().resolve($live_setting).promise();
			}

		}, function(result) {
			const [xhr, status, error] = result;
			console.error('AJAX通信エラー(XML)', xhr, status, error);
		});
	};

	/**
	 * 表示切替（TOPページ用）
	 */
	var switchTop = function(openStatus) {
    console.log("openStatus", openStatus)
    location.href = openStatus === DISP_MODE_OPEN ? 'https://tacos.co3.co.jp/users/team2022line1/login.aspx' : 'sorry.html';
		// location.href = openStatus === DISP_MODE_OPEN ? '../login.html' : 'sorry.html';

	}

	/**
	 * 表示切替（SORRYページ用）
	 */
	 var switchSorry = function(openStatus) {
		// オープン時に直打ち等でsorryページに来たときはログインページにリダイレクト
		// if(openStatus === DISP_MODE_OPEN ) {
			// location.href = 'https://tacos.co3.co.jp/users/team2022line1/login.aspx';
			// return false;
		// }

		// let txt = sorryText.text().trim();
		// $('[data-add-text="sorry"]').html(txt);
		$('[data-switch-button="sorry"]').attr('aria-disabled', openStatus !== DISP_MODE_OPEN)
	}

	/**
	 * 現在時刻から公開モード取得
	 * @param string now
	 * @param string open
	 * @param string close
	 */
	var getOpenStatus = function(now, open, close) {

		// 時間フォーマットが不正の場合、公開前として返す
		if (!(now &&
				chkTimeFormat(open) &&
				chkTimeFormat(close))
		) {
			return DISP_MODE_BEFORE;
		}

		// 強制閲覧モードチェック
		const forcedMode = Number(param[FORCED_BROWSE_KEY]);

		// 設定あった場合
		if (forcedMode) {
			// 存在するモードなら、そのまま返却
			if (
				[
					DISP_MODE_BEFORE,
					DISP_MODE_CLOSE,
					DISP_MODE_OPEN,
					DISP_MODE_DVR_CLOSE
				].includes(forcedMode)
			) {
				return forcedMode;
			}
		}

		// 時差表記の文字列で処理するのでnew Date使用で大丈夫
		const nowTime = new Date(now);
		const openTime = new Date(open);
		const closeTime = new Date(close);

		const nowMoment = nowTime.getTime();
		const openMoment = openTime.getTime();
		const closeMoment = closeTime.getTime();


    if (Number.isNaN(nowMoment) ||
			Number.isNaN(openMoment) ||
			Number.isNaN(closeMoment)) {
			return DISP_MODE_BEFORE;
		}

		// 公開前
		if (nowMoment < openMoment) {
			return DISP_MODE_BEFORE;
		}

		// 公開終了
		if (nowMoment >= closeMoment) {
			return DISP_MODE_CLOSE;
		}

		// 公開中
		return DISP_MODE_OPEN;
	};

	/**
	 * 時間文字列のフォーマットチェック
	 * YYYY/MM/DD HH:ii:ss (2021/7のブラッシュアップで使用不可)
	 * またはタイムゾーン（Ex: 1987-04-01T+0900）を許可
	 * @param string str
	 */
	var chkTimeFormat = function(str) {
		if (typeof str === 'string' &&
			(/*str.match(/^\d{4}\/\d{2}\/\d{2}\s\d{2}\:\d{2}\:\d{2}$/) ||*/
				str.match(/^(\d{1,4})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])T([0-1][0-9]|2[0-4]):[0-5][0-9]:[0-5][0-9](Z|(\+|-)([01][0-9]|2[0-4]):([0-5][0-9])?)$/))) {
			return true;
		}
		return false;
	};

	/**
	 * 各種リンクに強制閲覧モードセット
	 * （XMLの内容をHTMLに書き込みを行った後に実行する）
	 * @param string key
	 */
	 var setParmForcedMode = function() {
		// 強制閲覧モード
		const forcedMode = Number(param[FORCED_BROWSE_KEY]);

		// 存在しないモードの場合、スルー
		if (!(
				[
					DISP_MODE_BEFORE,
					DISP_MODE_CLOSE,
					DISP_MODE_OPEN,
					DISP_MODE_DVR_CLOSE
				].includes(forcedMode)
			)) return;

		$("." + FORCED_BROWSE_ADD_CLASS).attr("href", function(i, href) {
			const param = new Url(href, true).query;
			param[FORCED_BROWSE_KEY] = forcedMode;

			return href.split("?")[0] + qs.stringify(param, true);
		});
	};

	// ************************************************
	// publicモジュール
	// ************************************************
	return {
    // ************************************************
		// SORRY画面用 切替処理
		// ************************************************
		initSorry: function() {
      checkTime(TARGET_PAGE_SORRY);

			setInterval(function() {
				checkTime(TARGET_PAGE_SORRY);
			}, CHECK_TIMER);
		},

		// ************************************************
		// TOP画面用 切替処理
		// ************************************************
		initTop: function() {
      checkTime(TARGET_PAGE_TOP);
		},


	};
})();