const config = {
  // 管理者ID
  ADMINISTRATOR:'JSTLIVE',

  BBSID: 2,

  TARGET_DAY: '1',
  // 対象ページ
  TARGET_PAGE: {
    LOGIN: "LOGIN",
    SORRY: "SORRY",
    PLAYER: "PLAYER",
  },

  TARGET_PLAYER_DIR: '../',

  SETTING_XML_PATH: 'xml/setting.xml',

  FORCED_BROWSE_KEY: 'forced_mode',

  FORCED_BROWSE_ADD_CLASS: 'add_forced_mode',

  CHECK_TIMER: 10000,

  TICKER_SPEED: 80,

  DISP_MODE: {
    // 公開モード 公開前
    BEFORE: 1,
    // 公開モード 公開中
    OPEN: 2,
    // 公開モード 公開終了
    CLOSE: 3,
  }

};

export default config;