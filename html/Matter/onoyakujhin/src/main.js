import $, { fn } from "jquery";
import Cookies from "js-cookie";
import Config from "./config";
// import qs from "querystringify";
// import Url from "url-parse";
import TacosApi from './modules/tacos-api';
import BbsApi from './modules/bbs-api';
// import loadScript from "./modules/js-loadscript-jin-edition";
import {RealTimeEnquete} from "./modules/real-time-enquete";
/* ----------------------------------------
	初期設定
---------------------------------------- */

// 対象ページ Player
const TARGET_PAGE_PLAYER = "PLAYER";

const TARGET_PLAYER_DIR = '../';

const SETTING_XML_PATH = 'common_live/xml/setting.xml';



let goodCountDataArr = Cookies.get('goodCountData') ? JSON.parse(Cookies.get('goodCountData')) : [];
const GOOD_COUNT_DATA = {
	userId: "",
	data: goodCountDataArr
};

let intervalId = false;

// ****************
// 切り替え用 関数
// ****************
window.switchModel = (function() {
	// ************************************************
	// privateモジュール
	// ************************************************
	/**
	 * 時間チェック（メイン処理）
	 */
	var checkTime = function(target_page) {

		// plyaerページの場合、階層設定
		var xmlDir = target_page == TARGET_PAGE_PLAYER ? TARGET_PLAYER_DIR : '';
		xmlDir += SETTING_XML_PATH;

		return $.when(
		// xml取得
		$.ajax({
			type: 'GET',
			url: xmlDir,
			dataType: 'xml',
			cache: false,
			}),

			// DOM待ち
			$.ready

		).then(function(result) {
			const [xml, status, xhr] = result;

			// ライブの情報を取得
			const $live_setting = $(xml).find('live_setting');

			if ($live_setting.length) {

				return new $.Deferred().resolve($live_setting).promise();
			}

		}, function(result) {
			const [xhr, status, error] = result;
			console.error('AJAX通信エラー(XML)', xhr, status, error);
		});
	};

	var checkBbs = function() {
		return $.when(
			BbsApi.getBbsPosts().then(result => {
				// console.log("BbsApi", result);
				let dataPostList = result.data.post_list;

				/**
				 * 最新の質問表示処理
				*/

				// 最新投稿記事データ取得
				let latestPostData = getlatestPostData(dataPostList);
				// let tickerData = createTickerData(dataPostList);

				// console.log("tickerData", tickerData)

				// cookie取得
				const tickerIds = Cookies.get('tickerIds') ? JSON.parse(Cookies.get('tickerIds')): null;
				// console.log('tickerIds', tickerIds)

				const latestTickerIds = dataPostList.map(postItem => {
					return postItem.post_id;
				});
				// console.log("latestTickerIds", latestTickerIds);

				const comparison = JSON.stringify(tickerIds) === JSON.stringify(latestTickerIds);
				// console.log("comparison", comparison);

				if(!tickerIds || !comparison) {
					upDateTicker(dataPostList);
				}


				// cookie_ticker_idがない or cookie_ticker_idがある且つ、ticker_idとcookie_ticker_idが違う場合ticker更新
				// if(!getLatestPostCookie || getLatestPostCookie !== latestPostData.id) {
				// }

				// cookie設定
				Cookies.set('latestPostId', latestPostData.id, {secure: true, sameSite: "none"});


				/**
				 * 質問ランキング表示処理
				*/
				upDateRanking(dataPostList);
				// console.log("rankingjson", rankingjson);

				return {
					dataPostList,
					latestPostData,
				};
			})
		).then((data) => {
			return new $.Deferred().resolve(data).promise();
		})

	}


	let createTickerData = function(dataPostList) {
		const tickerIds = Cookies.get('tickerIds') ? JSON.parse(Cookies.get('tickerIds')): null;
		// const tickerIds = [2, 6, 4, 2,3];
		//[2, 6, 4, 3]
		const latestTickerIds = dataPostList.map(postItem => {
			return postItem.post_id;
		});

		// console.log("latestTickerIds", latestTickerIds);
		// console.log("tickerIds", tickerIds);
		const comparison = JSON.stringify(tickerIds) === JSON.stringify(latestTickerIds);
		// console.log("comparison", comparison);

		if(tickerIds && comparison) {
			console.log('何もしない')
			return;
		}

		const blanktext = '　'.repeat(5)
		const latestTickerMessages = dataPostList.map(postItem => {
			const message = postItem.message + blanktext;
			return message;
		}).filter(Boolean);

		Cookies.set('tickerIds', latestTickerIds, {secure: true, sameSite: "none"});

		return latestTickerMessages;
	}

	/**
	 * 最新投稿記事取得(公開順で最新のもの)
	 */
	let getlatestPostData = function(dataPostList = []) {
		const resultData = {
			id:null,
			message: null,
		}
		let latestPostJson = Object.assign([], dataPostList);
		let latestPostOrg = 	latestPostJson[0];
		return {
			id: latestPostOrg && latestPostOrg.post_id ? latestPostOrg.post_id : null,
			message: latestPostOrg && latestPostOrg.message ? latestPostOrg.message.replace(/\r?\n/g," "): null,
		}
	}

	/**
	 * ticker更新処理
	 */
	let upDateTicker = function(dataPostList) {
		// console.log('----- upDateTicker -----')
		// console.log('dataPostList', dataPostList)

		// const tickerIds = Cookies.get('tickerIds') ? JSON.parse(Cookies.get('tickerIds')): null;
		// console.log('tickerIds', tickerIds)

		const latestTickerIds = dataPostList.map(postItem => {
			return postItem.post_id;
		});
		// console.log("latestTickerIds", latestTickerIds);

		// const comparison = JSON.stringify(tickerIds) === JSON.stringify(latestTickerIds);
		// console.log("comparison", comparison);

		// if(tickerIds && comparison) {
		// 	console.log('何もしない')
		// 	return;
		// }

		const tickerMessageAll = false;
		const blanktext = '　'.repeat(5);
		let latestTickerMessages;
		let slicetext;
		let tickerMessage;
		if(tickerMessageAll) {
			latestTickerMessages = dataPostList.map(postItem => {
				slicetext = postItem.message.length > 40 ? (postItem.message).slice(0,40)+"…" : postItem.message;
				tickerMessage = slicetext + blanktext;
				return tickerMessage;
			}).filter(Boolean).join('');
		}else{
			// latestTickerMessages = dataPostList[0].message.length > 40 ? (dataPostList[0].message).slice(0,40)+"…" : dataPostList[0].message;
			latestTickerMessages = dataPostList[0].message;
		}


		// console.log("latestTickerMessages", latestTickerMessages);

		Cookies.set('tickerIds', latestTickerIds, {secure: true, sameSite: "none"});
		const latastPostTarget = $("#marquee");
		latastPostTarget.empty();
		let latestPostNewLine;
		const message = latestTickerMessages.replace(/\n/g, '');
		// console.log("message", message)
		if(message) {
			let $container = $('#ticker');
			let $outerWidth = $container.outerWidth();
			let $textWidth = (message.length * 18)
			let $speed;
			// if($outerWidth < $textWidth) {
			// 	$speed = "100";
			// }else{
			// 	$speed = $textWidth / $outerWidth * 70;
			// }
			// console.log("$outerWidth", $outerWidth)
			// console.log("textWidth", $textWidth)
			$speed = ($outerWidth + $textWidth) / Config.TICKER_SPEED;
			// document.documentElement.style.setProperty("--duration", `${Math.floor($speed)}s`)

			latastPostTarget.removeClass('inActive');
			latastPostTarget.addClass('isActive');
			latestPostNewLine = $(`<p>${message}</p>`);
			latestPostNewLine.css({"animationDuration": `${$speed}s`})
		}else{
			latastPostTarget.removeClass('isActive');
			latastPostTarget.addClass('inActive');
			latestPostNewLine = $(`<p>投稿記事がありません。</p>`);
		}


		latastPostTarget.append(latestPostNewLine);
	}

	/**
	 * 質問ランキング表示処理
	 */
	let upDateRanking = function(rankingData = {}) {
		let rankingjson = Object.assign([], rankingData);
		rankingjson.sort((a, b) => {
			if (a.likes > b.likes) {
				return -1;
			}
			if (a.likes < b.likes) {
				return 1;
			}
			return 0;
		});

		const rankingTarget = $('#ranking');

		// console.log("rankingjson", rankingjson)
		let rankingNewList;
		const newRankingJson = Object.assign([], rankingjson).splice(0, 10);
		// console.log("newRankingJson", newRankingJson)

		if(rankingjson.length) {
			rankingNewList = newRankingJson.map((item, index) => {
				let rankingMessage = item.message;
				let rankingImportance;
				switch(index) {
					case 0:
						rankingImportance = "primary";
						break;
					case 1:
						rankingImportance = "secondary";
						break;
					case 2:
						rankingImportance = "tertiary";
						break;
					default:
						rankingImportance = "common";
						break;
				}
				let rankingNumber = index + 1;
				let htmlTempate = `
				<li class="p-ranking_item p-ranking-${rankingImportance}" data-rank="${rankingNumber}">
					<div>
						${rankingMessage}
					</div>
				</li>
				`;

				return htmlTempate;
			}).filter(Boolean).join('');
		}else{
			rankingNewList = `
			<li>ランキングデータ準備中です。</li>
			`
		}

		rankingTarget.empty();
		rankingTarget.append(rankingNewList)
	}

	/**
	 * 投稿一覧表示
	 */
	let createPostList = function(postListData = []) {
		// console.log("createPostList", postListData);
		let hasContainer;
		let $list_container;
		if(!$('#postall').length) {
			hasContainer = false;
			// console.log('shokai')
			$list_container = $('<ol class="p-postall modal_inner" id="postall">');
		}else{
			hasContainer = true;
			// console.log('no shokai')
			$list_container = $('#postall');
		}
		const $list_item = postListData.map(item => {
			const liked = goodCountDataArr.includes(item.post_id) ? 'liked' : '';
			let htmlTempate = `
			<li class="p-postall_item">
				<div class="p-postall_text">${item.message.replace(/\n/g, '<br>')}</div>
				<div class="p-postall_good ${liked}" data-good="up" data-post-id="${item.post_id}"><span>${item.likes}</span></div>
			</li>
			`;

			return htmlTempate;
		}).filter(Boolean).join("");


		// console.log("hasContainer", hasContainer)
		if(hasContainer) {
			$list_container.html($list_item);
			return hasContainer
		}else{
			return $list_container.html($list_item);
		}
	}

		/**
	 * モーダル表示処理
	 */
	let createModal = function($this, dataPostList) {
		// $('[data-layer="warpper"]').remove();
		const type = $this.data('modalOpen');
		let modalTitle;
		let cloneTitle;
		let layer = $('<div class="modal_layer" data-layer="warpper">');
		let overlay = $('<div class="modal_layer_overlay" data-layer="close">');
		let container = $('<div class="modal_layer_container">');
		let content = $('<div class="modal_layer_content" data-layer="content">');
		let closeButton = $('<div class="modal_layer_closebutton" data-layer="close">');
		let contentBody = $('<div class="modal_layer_body">')
		content.append(container).append(closeButton);

		switch (type) {
			// 視聴ページ機能説明拡大表示
			case 'expansion':
				modalTitle = $(`[data-modal-title="${type}"]`);
				cloneTitle = modalTitle.clone(true);
				cloneTitle.addClass('modal_layer_title');
				container.append(cloneTitle);

				let expansionContent = $(`[data-modal-body="${type}"]`).clone(true).addClass('modal_inner');
				container.append(contentBody.append(expansionContent));
				break;

			// 投稿一覧表示
			case 'ticker':
			case 'ranking':
				// setPostList();
				var cb = function() {
					BbsApi.getBbsPosts().then((result)=>{
						let dataPostList = result.data.post_list;
						// console.log("postall", dataPostList)
						if(typeof createPostList(dataPostList) === 'object') {
							contentBody.append(createPostList(dataPostList))
							container.append(contentBody);
						}
					});
				}
				cb();
				startInterval(cb);
				modalTitle = $(`<div>`);
				modalTitle.text('投稿一覧')
				modalTitle.addClass('modal_layer_title');
				container.append(modalTitle);


				break;

		}

		layer.append(overlay).append(content);
		$("html").css({
			"height": "100%",
			"overflow": "hidden",
		})
		$("body").append(layer);

	}

	// setIntervalメソッドを実行し、繰り返し処理を開始する関数
	function startInterval(cb) {
		intervalId = setInterval(function() {
			// 実行させる処理を記述
			cb();
		}, Config.CHECK_TIMER);
	}

	// setIntervalメソッドの繰り返し処理を解除する関数
	function stopInterval() {
		clearInterval(intervalId);
	}


	// ************************************************
	// publicモジュール
	// ************************************************
	return {
		// ************************************************
		// PLAYER画面用 切替処理
		// ************************************************
		initPlayer: function() {
			TacosApi.getUserInfo()
			.then(result => {
				// console.log("---- TacosApi ----", result);
				const user = {
					checkkey1: result.checkkey1,
					mrInfo: result.freefield1 ? result.freefield1 : 'MRName|ONO|1|f001営業部|f001営業所',
					info1: result.freefield2 ? result.freefield2 : '医師|88888888|DrName',
					info2: result.freefield3 ? result.freefield3 : '999999999|不明',
					info3: result.freefield4 ? result.freefield4 : 'f004医院',
					info4: result.freefield5 ? result.freefield5 : '診療科|その他',
					tags: result.tags ? result.tags : 'tags',
				};
				Cookies.set('user', JSON.stringify(user), {expires: 1, secure: true, sameSite: "none"});

				// ユーザーIDを格納
				if(Cookies.get('user')) {
					GOOD_COUNT_DATA.userId = user.checkkey1;
				};
				// console.log("user.checkkey1", user.checkkey1)
				// イイネした記事のidを格納
				if(Cookies.get('goodCountData')) {
					GOOD_COUNT_DATA.data = JSON.parse(Cookies.get('goodCountData'));
				}

				// console.log("user", user);
				// console.log("GOOD_COUNT_DATA", GOOD_COUNT_DATA);

				checkTime(TARGET_PAGE_PLAYER)
				.then($live_setting => {


					// プレイヤーの書き出し
					let playerType = ""
					const href = location.href;
					switch (true) {
						case /live\/live/.test(href):
							playerType = 'live'
							// console.log("live");
							break;
						case /live\/dvr/.test(href):
							playerType = 'dvr'
							// console.log("dvr");
							break;
					}

					const $player = $live_setting.find(`livePlayer player[id="${playerType}"]`);
					const $environment = $live_setting.find('environment');
					const $normalEnquete = $live_setting.find('normalEnquete');
					const $realTimeEnquete = $live_setting.find('realTimeEnquete');
					const $bbs = $live_setting.find('bbs');

					if (!$player.length) {
						// 該当するプレイヤーがなければリダイレクト
						location.replace(TARGET_PLAYER_DIR + "index.html");
						return;
					}

					$player.attr("src", (_, src) => {
						$("#livePlayer").attr("src", src);
					});

					$realTimeEnquete.attr("src", (_, src) => {
						let connectorSymbol = /\?/.test(href) ? "&": "?";
						$("#realTimeEnquete").attr("src", src + connectorSymbol + 'op1=' + encodeURIComponent(result.checkkey1));
					});

					$normalEnquete.attr("src", (_, href) => {
						let connectorSymbol = /\?/.test(href) ? "&": "?";
						$("#normalEnquete").attr("href", href + connectorSymbol + 'op1=' + encodeURIComponent(result.checkkey1) + '&enqid=' + $normalEnquete.attr('id'));
					});

					// $bbs.attr("src", (_, src) => {
					// 	$("#bbs").attr("src", src);
					// });
					$bbs.attr("src", (_, src) => {
						let connectorSymbol = /\?/.test(src) ? "&": "?";
						// console.log("connectorSymbol", connectorSymbol)
						$("#bbs").attr("src", src + connectorSymbol + 'p=' + encodeURIComponent(result.checkkey1));
					});

					// お問い合わせ先 / 注意事項作成
					const envBlockTitle = $environment.find("title").text();

					$.each($('[data-environment]'), (_, item) => {
						let $envcontainer = $('<div class="notes">');

						$.each($environment.find("list"), (_, list) => {
							const $itemTitle = $(`<p class="notes_title">${$(list).attr('title')}</p>`)
							const $itemUl = $(`<ul class="${$(list).attr('class')}"></ul>`)
							$.each($(list).find('item'), (_, el) => {
								$itemUl.append(`<li>${$(el).text()}</li>`)
							})
							$envcontainer.append($itemTitle);
							$envcontainer.append($itemUl);
						});

						let $envwrapper = $('<div class="card-content">');

						const title = $(`<p>${envBlockTitle}</p>`)
						$(item).append(title)
						$(item).append($envwrapper)
						$envwrapper.append($envcontainer);
					})

					// sp表示時のタブ切り替え
					$('[data-show-name]').on('click', (e) => {
						$('body').attr('data-view-mode', '');
						const $showName = $(e.target).data('showName');
						$('body').attr('data-view-mode', $showName);

						$('[data-show-name]').removeClass('current');
						$(e.target).addClass('current');
					})


					// リアルタイムアンケート結果表示
					// 引数（[対象のdata属性], [iframeのパス]）
					const realTimeEnqueteResultSrc = $live_setting.find('realTimeEnqueteResult').attr("src");
					new RealTimeEnquete("enquete-result", realTimeEnqueteResultSrc);


					$(".loading").removeClass("loading");


				});

				checkBbs().then((data) => {
					// console.log("checkBbs", data);
					upDateTicker(data.dataPostList);

					$(document).on('click','[data-modal-open]',(e)=> {
						e.preventDefault();
						const $this = $(e.currentTarget);
						createModal($this, data.dataPostList);
					});
					$(document).on('click', '[data-layer="close"]', (e) => {
						e.preventDefault();
						$("html").css({
							"height": "auto",
							"overflow": "auto",
						});

						stopInterval();
						$('[data-layer="warpper"]').remove();
					});

					// いいねボタン押下処理
					$(document).on('click', '[data-good="up"]', (e) => {
						const $this = $(e.currentTarget);
						const $post_id = $this.data('postId');

						// 1記事に付き、１アカウント１クリックのみ（管理者は何回でも）
						// 管理者かどうか
						let administrator = GOOD_COUNT_DATA.userId === Config.ADMINISTRATOR;
						if(/adminmode\=on/.test(window.location)) {
							administrator = true;
						}else
						if(/adminmode\=off/.test(window.location)) {
							administrator = false;
						}

						const liked = goodCountDataArr.includes($post_id);
						if(!administrator) {
							// ユーザーIDが同じ且つ、いいね済かどうか。
							if(liked) {
								// alert("こちらの記事にはすでにいいねしています。");
								return
							}
						}

						// いいねした投稿IDを保持
						if(!liked) {
							goodCountDataArr.push($post_id);
						}

						BbsApi.setGoodPosts({post_id: $post_id}).then(good => {
							// いいねした記事が存在しない
							if(!good.result) {
								alert("投稿情報が存在しません")
								return;
							}

							// いいね数更新
							$this.addClass('liked');
							$this.find('span').text(good.data.likes);

							// cookieに保存
							Cookies.set('goodCountData',  JSON.stringify(goodCountDataArr), {expires: 1, secure: true, sameSite: "none"})

						}, result => {
							console.log(result)
						});
					})

				});

				setInterval(function() {
					checkBbs();
				},Config.CHECK_TIMER);
			})

		}

	};
})();