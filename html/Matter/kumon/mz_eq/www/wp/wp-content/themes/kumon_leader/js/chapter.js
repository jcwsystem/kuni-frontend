(function ($) {

  /**
   * DOM構築時に処理を開始する
   */
  $(function() {
    initialized();
  });

  /**
   * プレーヤーオブジェクトの入れ物
   */
  var player = {};

  /**
   * 初回フラグ
   * @type {Boolean}
   */
  var firstPlayFlag = true;

  /**
   * 待ち
   * @type {Number}
   */
  var seekReserved = -1;

  /**
   * 初期化
   */
  function initialized() {
    getPlayerObj();
    createChapter();
  }

  /**
   * eqPlayerのオブジェクトが出来たら返す
   * @returns {void}
   */
  function getPlayerObj() {
    let eqPlayer = window.eqPlayer;
    var id = setInterval(function () {
      if (eqPlayer) {
        player = eqPlayer;
        initPlayer(player);
        clearInterval(id);
      }
    }, 500);
  }

  /**
   * チャプターを作る
   * @returns {void}
   */
  function createChapter() {
    $('#time-controller').find('li').on('click', debounce(onClickHandler, 300));
  }

  /**
   * チャプターの時間をclass名から置換して返す
   * @param {HTMLElement} element
   * @returns {Number}
   */
  function getReplaceTime(element) {
    var klass = $(element).attr('class');
    var sec = klass.replace(/^time_/, '');
    return sec;
  }

  /**
   * クリック時に時間をセットする
   * @param {Event} e
   */
  function onClickHandler(e) {
    var sec = getReplaceTime(this);
    setSeekTime(sec);
  }

  /**
   * 指定の時間までジャンプする。プレーヤーが初回時に起動していない時は起動して、チャプターへジャンプする。
   * @param {Number} sec
   */
  function setSeekTime(sec) {
    if (firstPlayFlag) {
      seekReserved = sec;
      player.accessor.play();
    } else {
      player.accessor.setCurrentTime(sec);
    }
  }

  /**
   * 初回時にフラグを捨てる
   * @param {Object} player
   */
  function initPlayer(player) {
    player.accessor.addEventListener("playing", function () {
      if (firstPlayFlag) {
        firstPlayFlag = false;
      }
      if (seekReserved > -1) {
        setSeekTime(seekReserved);
        seekReserved = -1
      }
    });
  }

  /**
   * イベントの間引き
   * @param {Function} func
   * @param {Number} wait
   * @param {Boolean} immediate
   * @returns {Function}
   */
  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
          args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

})(jQuery);
