(function ($) {

  /**
   * DOM構築時に処理を開始する
   */
  $(function() {
    initialized();
  });

  /**
   * プレーヤーオブジェクトの入れ物
   */
  var player = {};

  /**
   * 初回フラグ
   * @type {Boolean}
   */
  var firstPlayFlag = true;

  /**
   * 待ち
   * @type {Number}
   */
  var seekReserved = -1;

  /**
   * 初期化
   */
  function initialized() {
    getPlayerObj();
    createChapter();
  }

  /**
   * JMCPlayerのオブジェクトが出来たら返す
   * @returns {void}
   */
  function getPlayerObj() {
    // var $target = $("#movie_script");
    // var $html = $('<div>').append($target.clone(true)).html();
    // var subStStart = $html.indexOf("b:");
    // // var subStEnd = $html.indexOf("s:{");
    // var subStEnd = $html.indexOf("s:{");
    // var subStResult = $html.substring( subStStart, subStEnd );
    // var arr = subStResult.replace(/\r?\n/g, "").replace(/\t/g, "").split(",").filter(Boolean);
    // let eqConfig = {};
    
    // arr.forEach((element, index) => {
    //   var el = element.split(':');
    //   console.log(index)
    //   console.log("forEach", element.split(':'))
    //   eqConfig[el[0]] = el[1].replace(/\"/g, "");
    // });
    // console.log(eqConfig)
    // eqConfig["s"] = {
    //   bskb:"10",
    //   cc:"off",
    //   el:"off",
    //   fskb:"10",
    //   hp:450,
    //   il:"off",
    //   ip:"on",
    //   mb:"off",
    //   pr:"1.2,1.5",
    //   prl:"off",
    //   rp:"fit",
    //   sn:"",
    //   tg:"off",
    //   ti:"off",
    //   wd:"2",
    //   wp:800
    // };
    // console.log("eqConfig", eqConfig);
    // // var dummyPlayer = $('<div id="dummyPlayer">');
    // // $("body").append(dummyPlayer);
    // window.eqPlayer = jstream_t3.PlayerFactoryIF.create(eqConfig, "movie_script")
    // console.log("eqPlayer", eqPlayer)
    // console.log("jstream_t3", jstream_t3.PlayerFactoryIF.iFrameElement)
    // var id = setInterval(function () {
    //   if (window.eqPlayer) {
    //     player = window.eqPlayer;
    //     initPlayer(player);
    //     clearInterval(id);
    //   }
    // }, 500);
    console.log(JMCPlayer)
    window.player.accessor.play();
    var id = setInterval(function () {
      if (JMCPlayer) {
        player = JMCPlayer.instance;
        initPlayer(player);
        clearInterval(id);
      }
    }, 500);
  }

  /**
   * チャプターを作る
   * @returns {void}
   */
  function createChapter() {
    $('#time-controller').find('li').on('click', debounce(onClickHandler, 300));
  }

  /**
   * チャプターの時間をclass名から置換して返す
   * @param {HTMLElement} element
   * @returns {Number}
   */
  function getReplaceTime(element) {
    var klass = $(element).attr('class');
    var sec = klass.replace(/^time_/, '');
    return sec;
  }

  /**
   * クリック時に時間をセットする
   * @param {Event} e
   */
  function onClickHandler(e) {
    var sec = getReplaceTime(this);
    console.log("getReplaceTime", sec)
    setSeekTime(sec);
  }

  /**
   * 指定の時間までジャンプする。プレーヤーが初回時に起動していない時は起動して、チャプターへジャンプする。
   * @param {Number} sec
   */
  function setSeekTime(sec) {
    if (firstPlayFlag) {
      seekReserved = sec;
      player.accessor.play();
    } else {
      player.accessor.setCurrentTime(sec);
    }
  }

  /**
   * 初回時にフラグを捨てる
   * @param {Object} player
   */
  function initPlayer(player) {
    player.accessor.addEventListener("playing", function () {
      console.log("playing")
      if (firstPlayFlag) {
        firstPlayFlag = false;
      }
      if (seekReserved > -1) {
        setSeekTime(seekReserved);
        seekReserved = -1
        console.log("seekReserved", seekReserved)

      }
    });
  }

  /**
   * イベントの間引き
   * @param {Function} func
   * @param {Number} wait
   * @param {Boolean} immediate
   * @returns {Function}
   */
  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
          args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

})(jQuery);
