function _download_redirect(url) {
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){
                var referLink = document.createElement('a');
                referLink.href = url;
                document.body.appendChild(referLink);
                referLink.click();
        } else {
                location.href = url;
        }
}