(function($) {
  /**
   * DOM構築時に処理を開始する
   */
  $(function () {
    initialized();
  });
  /**
   * プレーヤーオブジェクトの入れ物
   */
  var player = {};

  /**
   * 初回フラグ
   * @type {Boolean}
   */
  var firstPlayFlag = true;

  /**
   *
   * @type {Number}
   */
  var seekReserved = -1;

  /**
   * 初期化
   */
  function initialized() {
    getPlayerObj();
    createSeekButton();
  }

  /**
   * 指定の時間までジャンプする。プレーヤーが初回時に起動していない時は起動して、チャプターへジャンプする。
   * @param {Number} sec
   */
  function setSeekTime(sec) {
    var currentTime = player.accessor.getCurrentTime();
    if (firstPlayFlag) {
      seekReserved = currentTime + sec;
      player.accessor.play();
    } else {
      player.accessor.setCurrentTime(currentTime + sec);
    }
  }

  /**
   * eqPlayerのオブジェクトが出来たら返す
   * @returns {void}
   */
  function getPlayerObj() {
    let eqPlayer = window.eqPlayer;
    var id = setInterval(function () {
      if (eqPlayer) {
        player = eqPlayer;
        initPlayer(player);
        clearInterval(id);
      }
    }, 500);
  }

  /**
   * ボタンにイベントを追加
   * @return {void}
   */
  function createSeekButton() {
    $('#btn-prev, #btn-next').on('click', debounce(onClickHandler, 300));
  }

  /**
   * 押されたボタンによって時間を調整
   * @param {Event} e
   */
  function onClickHandler(e) {
    var btnName = $(this).attr('id');
    var position = btnName.replace(/^btn-/, '');
    switch (position) {
      case 'prev':
        setSeekTime(-10);
        break;
      case 'next':
        setSeekTime(10);
        break;
    }
  }

  /**
   * 初回時にフラグを捨てる
   * @param {Object} player
   */
  function initPlayer(player) {
    player.accessor.addEventListener("playing", function () {
      if (firstPlayFlag) {
        firstPlayFlag = false;
      }
      if (seekReserved > -1) {
        setSeekTime(seekReserved);
        seekReserved = -1
      }
    });
  }

  /**
   * イベントの間引き
   * @param {Function} func
   * @param {Number} wait
   * @param {Boolean} immediate
   * @returns {Function}
   */
  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this,
          args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

})(jQuery);
