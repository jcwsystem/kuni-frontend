(function () {

  /**
   *ビデオ聴講レポートを提出する
   */
  $(function() {

      $("#movieReportReport").keyup(countSize);
      $("#movieReportReport").keyup();

      $("#videoForm").click(function(e) {
          var report = $("#movieReportReport").val().replace(/(^[\s　]+)|([\s　]+$)/g, ""); //trim;
          if (!report) {
              alert("レポート内容をご記入ください。");
              return false;
          }

          if (window.confirm("レポートを提出します。よろしいですか？")) {

              $.ajax({
                  type: 'POST',
                  url: '/api/Form/send',
                  data: $('#movieReport').serializeArray(),
                  success: function(data, textStatus, jqXHR) {
                      var jsonObj = $.parseJSON(data);
                      $('#movieReportReport').val('');
                      $('#movieReportReportCounter').html('0');
                      alert(jsonObj.message);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                      alert(jsonObj.message);
                  }
              });
          }
      });

  });


  function countSize() {
      var str = $(this).val().replace(/\r?\n/g,'');
      var len = str.length;
      $("#"+$(this).attr('id')+'Counter').html(len);
  } 

})();
