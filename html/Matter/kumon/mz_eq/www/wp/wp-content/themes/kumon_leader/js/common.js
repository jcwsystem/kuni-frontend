(function($){
    function disp(url){
        window.open(url,"window_name","width=400,height=400,scrollbars=no,resizable=no");
    }
    $(function(){

        /**
         * #2939 本文内のAタグで、「href="*?kmn_use_disp"」とすることで「class="use_disp"」を追加する
         */
        $('#contents-center .contents-center-box-know .textblock-p a').each(function(){
            var keywords = ['?kmn_use_disp','&kmn_use_disp'] ;
            var url = $(this).attr('href');
            for( var i in keywords) {
                if ( url.lastIndexOf(keywords[i]) > 0 ) {
                    $(this).attr('href', url.replace(keywords[i], '')).addClass('use_disp') ;
                    break;
                }
            }
        })

        $('.use_disp').on('click',function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            if(href){
                disp(href);
            }
        });

    /**
    *よくある質問（OKWeb）
    */
    /* Aタグからgroup id とURL取得 */
    //var group = $(".okweb").attr('data-group');
    //var action = $(".okweb").attr('data-action');
    //$('#okweb_form').attr('action',action);
    //$('input[name="sso_group_id"]').attr('value',group);

    $(".okweb").click(function(e){
            if(doit()==false) {
                return false;
            }
            var f = document.f;
            bc=document.charset;
            document.charset='shift_jis';
            f.submit();
            document.charset=bc;
    });
        function doit() {
            var f = document.f;
            var data= "";
            if(checkParam()==false) {
                return false;
            }
            data = makeMD5Param("euc-jp");

            f.sso_chk.value = MD5_hexhash(data);
        }
        function makeParam(encoding) {
            var f = document.f;
            if(encoding == "shift-jis"){
                data =
                    "sso_auth_key="+ f.sso_auth_key.value +"&"
                    +"sso_mail="+ f.sso_mail.value +"&"
                    +"sso_expired="+ f.sso_expired.value +"&"
                    +"sso_name="+ EscapeSJIS(f.sso_name.value) +"&"
                    +"sso_group_id="+ f.sso_group_id.value +"&"
                    +"sso_account="+ f.sso_account.value +"&"
                    +"sso_client_user_attr6="+ EscapeSJIS(f.sso_client_user_attr6.value) +"&"
                    +"sso_client_user_attr7="+ EscapeSJIS(f.sso_client_user_attr7.value) +"&"
                    +"sso_client_user_attr8="+ EscapeSJIS(f.sso_client_user_attr8.value) +"&"
                    +"sso_client_user_attr9="+ f.sso_client_user_attr9.value +"&"
                    +"sso_client_user_attr10="+ f.sso_client_user_attr10.value;
            } else {
                data=
                    "sso_auth_key="+ f.sso_auth_key.value +"&"
                    +"sso_mail="+ f.sso_mail.value +"&"
                    +"sso_expired="+ f.sso_expired.value +"&"
                    +"sso_name="+ f.sso_name.value +"&"
                    +"sso_group_id="+ f.sso_group_id.value +"&"
                    +"sso_account="+ f.sso_account.value +"&"
                    +"sso_client_user_attr6="+ f.sso_client_user_attr6.value +"&"
                    +"sso_client_user_attr7="+ f.sso_client_user_attr7.value +"&"
                    +"sso_client_user_attr8="+ f.sso_client_user_attr8.value +"&"
                    +"sso_client_user_attr9="+ f.sso_client_user_attr9.value +"&"
                    +"sso_client_user_attr10="+ f.sso_client_user_attr10.value;
            }
            return data;
        }
        function makeMD5Param(encoding) {
            var data= makeParam();
            data = data + "&sgr10on3sqppel";
            var jis_data = _from_sjis(data);
            if(encoding == "euc-jp") {
                data = _to_euc(jis_data);
            } else if(encoding == "shift-jis") {
                data = _to_sjis(jis_data);
            } else if(encoding == "utf-8") {
                data = _to_utf8(jis_data);
            }
            return data;
        }
        function checkParam() {
            var f = document.f;
            if(f.sso_auth_key.value == ""
            || f.sso_mail.value == ""
            || f.sso_expired.value == "") {
                alert("識別キー・メールアドレス・有効期限は必須項目です。");
                return false;
            }
        }

        /**
        *不二印刷
        */
        $('.fuji').on('click',function(e){
            document.fujisso.submit();
        });

        /**
         * trust form
         * KMN-287 [指]指導者にアンケートができるように仕組みを、なれっじに作りたい
         */
        $('div#trust-form .subject:has(span.require)').each(function(){
            $(this).find('span.require').hide();
            $(this).html($(this).html()+'<span class="kmn-require">(必須)</span>');
        }) ;
    });
})(jQuery);