// 追加するさいに楽をしたいので、jsonファイルから案件名やリンクなどを設定できるようにするためのjavascriptを書いているよ
const NAME = {
  target: "[data-development-list]",
  header: "index-header",
  blockContainer: "index-flex-col-item",
  list: "index-lists",
  listwrap: "index-listBlock",
  listTitle: "index-listBlock-title",
}

$(() => {
  $.getJSON("./common/json/developmentList.json", (json) => {
    const htmls = json.map(data => {
      const html_blockContainer = $(`<div class=${NAME.blockContainer}></div>`);

      return html_blockContainer
        .append(html_genre_info(data.genre))
        .append(html_list_block(data.contents));
    });

    $(NAME.target).append(htmls);
  });

  function html_genre_info(genre) {
    return `
      <p class=${NAME.header}>${genre}</p>
    `;
  }

  function html_list_block(contents) {
    return contents.map(data => {
      let contentsTitle = data.title;
      let html_info = `
        <p class=${NAME.listTitle}>${contentsTitle}</p>
      `;
      let html_constntsItem = data.item.map(itemData => {
        return `
          <li><a href="${itemData.link}">${itemData.title}</a></li>
        `;
      })

      const html_list = $(`<ul class=${NAME.list}>`);
      const html_wrap = $(`<div class=${NAME.listwrap}>`);

      html_list.append(html_constntsItem);
      html_wrap.append(html_info).append(html_list);

      return html_wrap;
    });
  }

})