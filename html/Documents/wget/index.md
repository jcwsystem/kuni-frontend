# wget

## wgetコマンド一覧
[https://atmarkit.itmedia.co.jp/ait/articles/1606/20/news024.html](https://atmarkit.itmedia.co.jp/ait/articles/1606/20/news024.html)


## 自分の環境で使えるwgetのやり方

### dev jcwにSSHでログイン
- ssh 'jcw@52.196.19.15'<br>
- jcwyay123<br>

ログインすると、「/home/jcw」のディレクトリにいるので、
- cd htdocs/kunishima/wget <br>
で、移動し、格納した案件ディレクトリに移動。なければ作成

ログインしたら、欲しいデータをdevjcw内に一旦落としてから、その後で、FTPを通して、ローカルにダウンロードする。
ベーシック認証や、それ以外の認証設定があるサイトのデータの落とし方がわからないので、調べましょう！！！！

### wgetコマンドでほしいソースをダウンロードする
- wget -p xxxxxxxxxxxxxxxx(サイトのパス)
※　-p そのHTMLの表示に使用しているファイルをあわせてダウンロードできます


------

