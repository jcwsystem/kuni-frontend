class EventTouch {
  constructor() {
      if (EventTouch.instance) return;
      EventTouch.instance = this;
      // -----------------------------
      this.isDown = false; //PC用

      this.currentPoint = new Point();
      this.startPoint = new Point();
      this.diffPoint = new Point();

      this.touchesdiff = [];
      this.touchesStartPoints = [new Point(), new Point()];
      this.touchesCurrentPoints = [new Point(), new Point()];

      this.scale = 1;

      // PC
      document.addEventListener("mousedown", this.onStart.bind(this), false);
      document.addEventListener("mousemove", this.onMove.bind(this), false);
      document.addEventListener("mouseup", this.onUp.bind(this), false);
      // SP
      document.addEventListener("touchstart", this.onStart.bind(this), false);
      document.addEventListener("touchmove", this.onMove.bind(this), false);
      document.addEventListener("touchend", this.onUp.bind(this), false);

      document.addEventListener("gesturechange", this.onGestureChange.bind(this), false);
  }

  originalEvent(e) {
      // jquery使用時用にoriginalEvent取得
      return (e = e.originalEvent ? e.originalEvent : e);
  }
  mouseDelta(e) {
      return e.deltaY ? -(e.deltaY) : e.wheelDelta ? e.wheelDelta : -(e.detail);
  }

  clientPosition(e) {
      this.originalEvent(e);
      if (e.touches) {
          return new Point(e.touches[0].clientX, e.touches[0].clientY);
      } else {
          return new Point(e.clientX, e.clientY);
      }
  }

  onStart(e) {
      this.originalEvent(e);
      this.isDown = true;
      this.diffPoint = new Point();
      this.startPoint = this.clientPosition(e);

      if (!e.touches) return;
      for (let i = e.touches.length - 1; i >= 0; i--) {
          this.touchesStartPoints[i] = new Point(e.touches[i].clientX, e.touches[i].clientY);
      }
  }

  onMove(e) {
      this.originalEvent(e);
      this.currentPoint = this.clientPosition(e);

      if (!this.isDown) return;
      this.diffPoint = new Point(this.currentPoint.x, this.currentPoint.y).subtract(this.startPoint);

      if (!e.touches) return;
      for (let i = e.touches.length - 1; i >= 0; i--) {
          this.touchesCurrentPoints[i] = new Point(e.touches[i].clientX, e.touches[i].clientY);
      }
  }

  onUp() {
      this.isDown = false;
  }

  onGestureChange(e) {
      this.originalEvent(e);
      let scale = 1;
      if (e.scale) {
          scale = e.scale;
      } else {
          const startDistance = Point.distance(this.touchesStartPoints[0], this.touchesStartPoints[1]);
          const currentDistance = Point.distance(this.touchesCurrentPoints[0], this.touchesCurrentPoints[1]);
          scale = currentDistance / startDistance;
      }
      this.scale = scale;
  }
}