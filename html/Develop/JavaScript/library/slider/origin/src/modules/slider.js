class Slider {
  constructor() {
      // -----------------------------
      this.slider = document.querySelector(".slider");
      this.slider__list = document.querySelector(".slider__list");
      this.slider__main = document.querySelector(".slider__main");
      this.slider__prev = document.querySelector(".slider__prev");
      this.slider__next = document.querySelector(".slider__next");
      this.slider__zoom = document.querySelector(".slider__zoom");
      this.slider__elm = document.querySelectorAll(".slider .slider__elm");
      // -----------------------------
      this.state = "ready";
      this.currentID = 1;
      // -----------------------------
      this.factor = 0.15; // イージング 係数
      this.fraction = 0.001; // スライド切り替え終点位置の繰り上げ・切り下げ 端数
      this.sliderWidth = 0; //スライド自体の幅
      this.responseRatio = 0.1; // mouse＆touchmove時にスライドorもと位置に戻すかの判定
      // -----------------------------
      this.currentPoint = new Point(); // 現在位置
      this.targetPoint = new Point(); // 目標位置
      // -----------------------------
      this.resize();
      this.events();
      this.elements();
      this.update();
  }

  events() {
      this.slider__prev.addEventListener("click", () => {
          this.slide("prev");
      }, false);
      this.slider__next.addEventListener("click", () => {
          this.slide("next");
      }, false);
      // this.slider__zoom.addEventListener("click", () => {
      //     Modal.instance.open(this.currentID);
      // }, false);

      this.slider__main.addEventListener("mousemove", this.onMove.bind(this), false);
      this.slider__main.addEventListener("touchmove", this.onMove.bind(this), false);
      this.slider__main.addEventListener("mouseup", this.onUp.bind(this), false);
      this.slider__main.addEventListener("touchend", this.onUp.bind(this), false);

      window.addEventListener("resize", this.resize.bind(this), false);
  }

  resize() {
      this.sliderWidth = this.slider.clientWidth;
      Object.assign(this.slider__main.style, {
          transform: `translate(${-this.sliderWidth}px, 0px)`
      });
  }

  onMove() {
      if (this.state != "ready") return;
      if (!EventTouch.instance.isDown) return;
      this.targetPoint.x = EventTouch.instance.diffPoint.x;
  }

  onUp() {
      if (this.state != "ready") return;
      const ratio = Math.abs(EventTouch.instance.diffPoint.x) / this.sliderWidth;
      if (ratio < this.responseRatio) {
          this.targetPoint.x = 0;
          return;
      }
      const state = (Math.sign(EventTouch.instance.diffPoint.x) != 1) ? "next" : "prev";
      this.slide(state);
  }

  clear() {
      this.currentPoint = new Point();
      this.targetPoint = new Point();
      this.state = "ready";
  }

  elements() {
      const slider__elm = document.querySelectorAll(".slider .slider__elm");
      const firstNode = slider__elm[0];
      const lastNode = slider__elm[slider__elm.length - 1];

      if (this.state == "next") {
          this.slider__list.append(firstNode);
      } else {
          this.slider__list.insertBefore(lastNode, firstNode);
      }
  }

  slide(_state) {
      if (this.state != "ready") return;
      this.state = _state;

      if (_state == "prev") {
          this.targetPoint.x = this.sliderWidth;
          this.currentID--;
      }
      if (_state == "next") {
          this.targetPoint.x = -this.sliderWidth;
          this.currentID++;
      }

      if (this.currentID < 1) this.currentID = this.slider__elm.length;
      if (this.currentID > this.slider__elm.length) this.currentID = 1;
  }

  update() {
      window.requestAnimationFrame(this.update.bind(this));

      this.currentPoint.x += Number(this.targetPoint.x - this.currentPoint.x) * this.factor;
      const ratio = Math.abs(this.currentPoint.x) / this.sliderWidth;

      if (ratio > (1.0 - this.fraction)) {
          this.currentPoint.x = this.targetPoint.x;
          this.elements();
          this.clear();
      }
      if (ratio < this.fraction) {
          this.currentPoint.x = this.targetPoint.x = 0;
      }

      Object.assign(this.slider__list.style, {
          transform: `translate(${this.currentPoint.x}px, 0px)`
      });
  }
}