class Point {
  constructor(x = 0, y = 0) {
      this.x = x || 0;
      this.y = y || 0;
  }
  static distance(a, b) {
      const dx = a.x - b.x;
      const dy = a.y - b.y;
      return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
  }
  static interpolate(p1, p2, t) {
      const x = p1.x * (1 - t) + p2.x * t;
      const y = p1.y * (1 - t) + p2.y * t;
      return new Point(x, y);
  }
  add(p) {
      this.x += p.x;
      this.y += p.y;
      return this;
  }
  subtract(p) {
      this.x -= p.x;
      this.y -= p.y;
      return this;
  }
  clone() {
      return new Point(this.x, this.y);
  }
}