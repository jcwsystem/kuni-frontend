const path = require("path");
const webpack = require("webpack");

module.exports = {
  // モード値を production に設定すると最適化された状態で、
  // development に設定するとソースマップ有効でJSファイルが出力される
  mode: "development",

  devServer: {
    contentBase: path.resolve(__dirname, "html"),
    open: true,
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          presets: [
            [
              "@babel/preset-env",
              {
                useBuiltIns: "usage",
                corejs: 3,
                // 'modules': false,
                // 'targets': {'browsers': ['ie >= 11']}
              },
            ],

            "@babel/preset-react",
          ],
        },
      },
      {
        test: /\.css/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: { url: false },
          },
        ],
      },
    ],
  },

  // plugins: [
  //   new webpack.ProvidePlugin({
  //     $: 'jquery',
  //     jQuery: 'jquery'
  //   })
  // ],

  // メインとなるJavaScriptファイル（エントリーポイント）
  // entry: './src/index.js',
  entry: {
    // 'index': './src/index.js',
    "./html/js/slider": "./src/slider.js",
  },

  // ファイルの出力設定
  output: {
    //  出力ファイルのディレクトリ名
    path: __dirname,
    // 出力ファイル名
    filename: "[name].js",
  },
  devtool: "source-map",
};
