(function() {
  document.addEventListener("DOMContentLoaded", () => {
    const config = {
      slidesToShow: 3,
    }

    const slide_wrap = document.querySelector('[data-slider="slider01"]');
    const slide_item = document.querySelectorAll('.slider-item');
    const slide_nav = document.querySelectorAll('[data-slide-nav]');
    const slide_pagination = document.querySelector('[data-slide-pagination]');

    let itemLength = slide_item.length;
    let itemCount = 0;

    let itemWidth = 0;
    // スライドコンテンツの幅取得
    const slide_contents_width = slide_wrap.clientWidth;
    console.log(itemLength)

    console.log(slide_contents_width / config.slidesToShow)

    slide_item.forEach((item, count) => {
      itemWidth = slide_contents_width / config.slidesToShow
      item.style.width = `${itemWidth}px`;
      item.dataset.itemCount = `${count}`
      // item.style.position = `absolute`;
      // item.style.left = `${count * (slide_contents_width / config.slidesToShow)}px`;
    });


    /**
     * 左右スライドボタン処理
     */
    slide_nav.forEach((btn) => {
      btn.addEventListener('click', (e) => {
        switch(e.target.dataset.slideNav) {
          case 'prev':
            if(itemCount > 0){
              itemCount --;
            }else{
              itemCount = (itemLength - 1)
            }
            break;
          case 'next':
            if(itemCount >= 0 && itemCount !== (itemLength - 1)){
              itemCount ++;
            }else{
              itemCount = 0
            }
            break;
        }
        slide_wrap.children[0].style.transform = `translate3d(${-itemWidth * itemCount}px, 0px, 0px)`;
      })
    })

    /**
     * ページネーション処理
     */
    Array.from(slide_pagination.children).forEach((item, count)=> {
      item.dataset.itemCount = `${count}`

      item.addEventListener('click', (e) => {
        itemCount = count;
        slide_wrap.children[0].style.transform = `translate3d(${-itemWidth * itemCount}px, 0px, 0px)`;
      })
    })

  });
})()