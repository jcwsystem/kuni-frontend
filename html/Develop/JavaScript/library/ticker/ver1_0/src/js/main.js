import $ from "jquery";
import Cookies from "js-cookie";
import qs from "querystringify";
import BbsApi from '../modules/bbs-api';

/* ----------------------------------------
	初期設定
---------------------------------------- */
const PARAM = qs.parse(location.search);
const API_PARAM = {
	url: PARAM.url,
	bbs_id: Number(PARAM.bbsid),
}
const OPTIONS = JSON.parse(PARAM.option);
// console.log("OPTIONS", OPTIONS)
// const CHECKTIMER = OPTIONS.checktimer && OPTIONS.checktimer.length? Number(OPTIONS.checktimer): 10;
const CHECKTIMER = 10000;

const tickerIdsCookieName = "ticker3rdids";

window.ticker = (function() {
	// ************************************************
	// privateモジュール
	// ************************************************

	let checkBbs = function() {
		return $.when(
			BbsApi.getBbsPosts(API_PARAM).then(result => {
				let dataPostList = result.data.post_list;

				/**
				 * 最新の質問表示処理
				*/

				// cookie取得
				const tickerIds = Cookies.get(tickerIdsCookieName) ? JSON.parse(Cookies.get(tickerIdsCookieName)): null;
				const latestTickerIds = dataPostList.map(postItem => {
					return postItem.post_id;
				});
				const comparison = JSON.stringify(tickerIds) === JSON.stringify(latestTickerIds);
				// console.log("tickerIds", tickerIds)
				// console.log("latestTickerIds", latestTickerIds)
				// console.log("comparison", comparison)

				/**
				 * 質問ランキング表示処理
				*/
				if(!tickerIds || !comparison) {
					upDateTicker(dataPostList);
				}

				return {
					dataPostList,
				};
			})
		).then((data) => {
			return new $.Deferred().resolve(data).promise();
		})

	}

	/**
	 * ticker更新処理
	 */
	let upDateTicker = function(dataPostList) {

		const latestTickerIds = dataPostList.map(postItem => {
			return postItem.post_id;
		});

		const tickerMessageAll = false;
		const blanktext = '　'.repeat(5);
		let latestTickerMessages;
		let slicetext;
		let tickerMessage;
		if(tickerMessageAll) {
			latestTickerMessages = dataPostList.map(postItem => {
				slicetext = postItem.message.length > 40 ? (postItem.message).slice(0,40)+"…" : postItem.message;
				tickerMessage = slicetext + blanktext;
				return tickerMessage;
			}).filter(Boolean).join('');
		}else{
			// latestTickerMessages = dataPostList[0].message.length > 40 ? (dataPostList[0].message).slice(0,40)+"…" : dataPostList[0].message;
			latestTickerMessages = dataPostList[0].message;
		}


		Cookies.set(tickerIdsCookieName, latestTickerIds, {secure: true, sameSite: "none"});

		const message = latestTickerMessages.replace(/\n/g, '');
		const $container = $("#ticker");
		const $fontSize = Number($container.css('fontSize').replace('px', ''));
		let latestPostNewLine;

		if(message) {
			let $outerWidth = $container.outerWidth();
			let $textWidth = (message.length * $fontSize);
			const speedParam = OPTIONS && OPTIONS.speed ? Number(OPTIONS.speed) : 70;
			const $speed = ($outerWidth + $textWidth) / speedParam;


			const props = {
				"--fontsize": OPTIONS && OPTIONS.fontsize && OPTIONS.fontsize.length ? `${OPTIONS.fontsize}px` : null,
				"--color": OPTIONS && OPTIONS.fontcolor && OPTIONS.fontcolor.length ? `${OPTIONS.fontcolor}` : null,
				"--padding": OPTIONS && OPTIONS.padding && OPTIONS.padding.length ? `${OPTIONS.padding}px` : null,
			}

			const newPorps = {}
			$.each(Object.keys(props), (_, prop) => {
				if(!props[prop]) return true;
				newPorps[prop] = props[prop];
			});

			$.each(Object.keys(newPorps), (_, prop)=>{
				document.documentElement.style.setProperty(prop, newPorps[prop]);
			});

			$container.removeClass('inActive');
			$container.addClass('isActive');
			latestPostNewLine = $(`<p>${message}</p>`);
			latestPostNewLine.css({"animationDuration": `${$speed}s`})

		}else{
			$container.removeClass('isActive');
			$container.addClass('inActive');
			latestPostNewLine = $(`<p>投稿記事がありません。</p>`);
		}

		$container.html(latestPostNewLine);
	}

	return {
		// ************************************************
		// ticker init
		// ************************************************
		init: function() {
			checkBbs().then((data) => {
				upDateTicker(data.dataPostList);

			});
			setInterval(function() {
				checkBbs();
			},(CHECKTIMER));

		}

	};
})();