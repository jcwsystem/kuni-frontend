import "../css/ticker.css";

import $ from "jquery";
import qs from "querystringify";

(function () {
  function setTicker(data) {
		$(window).on("load", () => {
			const iframe_src = window.tickerpath || "";
			const $wrap = $('<div class="ticker_container">')
			const $iframe = $(`<iframe id="ticker_frame" src="${iframe_src}" frameborder="0" allowfullscreen>`);

			switch(true) {
				case (!data.target || data.target === '') :
					alert('targetを設定してください');
					return;

				case !$(`#${data.target}`).length:
					alert('対象が見つかりません');
					return;

				case (!data.bbsid || data.bbsid === '') :
					alert('bbsidを設定してください');
					return;
				}

			$iframe.attr("src", (_, val) =>
				val + qs.stringify({
					bbsid: data.bbsid,
					option: data.option && typeof Object ? JSON.stringify(data.option): '{}',
				},true)
			);

      $(`#${data.target}`).css({'boxSizing': 'content-box'}).append($wrap.append($iframe));

			const contentHeight = data.option && data.option.fontsize && data.option.fontsize.length ? data.option.fontsize: null;
			if(contentHeight) {
				document.documentElement.style.setProperty("--height", `${data.option.fontsize}px`);
				$(".ticker_container").css({height: `${data.option.fontsize * 1.56}px`})
			}

		});
  }

  window.setTicker = setTicker;
})();