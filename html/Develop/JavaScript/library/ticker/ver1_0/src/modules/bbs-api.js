import $ from "jquery";

class BbsApi {
	constructor() {
		this.apiBaseUrl = 'https://d1ox3279py5nvx.cloudfront.net/api/post/';

		this.json;

		// リザルトコード（配列）
		this.resultCode = {
			ok: ['200'],
			ng: ['500']
		};

		// デバッグモード
		this.href = location.href;
		this.debugMode =  /localhost|dev\./.test(this.href);
	}

	request(url, param, type="GET") {
		const d = new $.Deferred();

		$.ajax({
			type: type,
			url: url,
			data: param,
			cache: false,
			dataType: 'json'
		})
		.done((json) => {
			if ($.inArray(json.resultCode, this.resultCode.ok)) {
				// OK
				this.json = json;
				d.resolve(json);
			} else {
				// NG
				console.error('AJAX通信エラー(TACOS)', json.resultCode);
				d.reject();
			}
		})
		.fail((xhr, status, error) => {
			console.error('AJAX通信エラー(API)', xhr, status, error);
			d.reject();
		})

		return d.promise();
	}

	getApiParam(param = {}) {
		return Object.assign({}, {bbs_id: param.bbs_id});
	}

	getBbsPosts(param = {}) {
		const url = this.apiBaseUrl + 'list';
		return this.request(url, this.getApiParam(param), "GET");
	}

}

export default new BbsApi();