/* --------------------------------------------------
	JS動的読み込み
-------------------------------------------------- */
// https://github.com/pavex/js-loadscript
// オプションで文字コードを指定できるように改造

export default function loadScript(src, option) {
	return new Promise((resolve, reject) => {
		try {
			let el = document.createElement('script');
			el.type = 'text/javascript';
			el.async = option && option.async ? option.async : true;
			el.onload = () => {
				resolve();
			};
			el.onerror = (e) => {
				throw e;
			};
			el.src = src;
			document.head.appendChild(el);
		}
		catch (e) {
			reject(e);
		}
	});
};