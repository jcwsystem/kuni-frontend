# イベントの構造化データ

検索結果画面にイベントのリッチリザルトを表示する方法。

■参考
- https://cluster-seo.com/blog/structured-data-for-event.html
- https://developers.google.com/search/docs/advanced/structured-data/event?hl=ja
- https://developers.google.com/search/docs/advanced/structured-data/intro-structured-data?hl=ja#structured-data
- https://developers.google.com/search/docs/advanced/structured-data/sd-policies?hl=ja

■リッチリザルトテスト
- https://search.google.com/test/rich-results?hl=ja
- https://support.google.com/webmasters/answer/7552505?hl=ja