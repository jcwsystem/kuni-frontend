kunishima front end wrorks 仕様書

■何がしたくて作るか。
・過去に作業した案件を記録しておくことで、似たような案件に対して素早く対応できるようにする。
・案件ごとの月別や年別開催など、バージョンが細かく分かれている場合に、どのバージョンを使えばよいか、わかるようにする。
・参考案件などを探し回らなくても良いようにしたいため。

■どのような形にするか。
・じんさんのfront end worksを参考にして、開発したもの、案件作業ものを一緒に入れておく。
・案件に関しては別途Gitなどがあるので、ここにはソースは置かない。その代わり、GitのURLやサイトのURL、また、作業内容をまとめて記録しておく。
・開発物に関しては、ここでソース管理し、アップデートを繰り返していく。ページもローカルで見られるようにする。

■管理方法
・データはローカルとGit（bitbucket）でソース管理。

■構成
index 
 | - 案件index
 |	|
 |	| - 案件
 |
 | - 開発index
 |	|
 |	| - 開発物A
 | 	| - 開発物B

■運用目標
一個のものを作成し始めたら、完成するまでやる。途中で切り上げない。または、いつまでに作るために、一日の作業目標をきめて作成する。



