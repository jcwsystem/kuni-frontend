const gulp              = require('gulp'),
      plumber           = require('gulp-plumber'), //エラー時の強制終了を防止
      notify            = require('gulp-notify'), //エラー発生時にデスクトップ通知する
      header            = require('gulp-header'),
      rename            = require("gulp-rename"), //.ejsの拡張子を変更
      replace           = require('gulp-replace'), // gulp上で置換が行える
      sourcemaps        = require('gulp-sourcemaps'),
      mode              = require('gulp-mode')({
                            modes: ["publish", "development"],
                            default: "development",
                            verbose: false
                          });
// css系
const sass              = require('gulp-sass'), //Sassコンパイル
      postcss           = require('gulp-postcss'), //autoprefixerとセット
      autoprefixer      = require('autoprefixer'), //ベンダープレフィックス付与
      flexBugsFixes     = require('postcss-flexbugs-fixes'),
      mqpacker          = require('css-mqpacker');

      
// Sassコンパイル
function styles() {
  return gulp.src([`./src/scss/**/*.scss`,`!./src/scss/**/_*.scss`])
  .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }) )//エラーチェック
  .pipe(mode.development(sourcemaps.init()))
  .pipe(sass({outputStyle: "expanded"}))
  .pipe(
    plumber({
      errorHandler: notify.onError('<%= error.message %>'),
    }),
  )
  .pipe(postcss([mqpacker()]))
  .pipe(postcss([
    flexBugsFixes,
    autoprefixer(
      {
        cascade: false,
        grid: "autoplace"
      }
    ),
  ]))
  .pipe(replace(/@charset "UTF-8";/g, ''))
  .pipe(header('@charset "UTF-8";\n\n'))
  .pipe(mode.development(sourcemaps.write('./')))
  .pipe(gulp.dest('./html/common/css/'))
}

function watchFiles() {
  gulp.watch([`./src/scss/**/*.scss`], gulp.series(styles))
}

/*
* タスクの実行
*
*/
exports.default = gulp.series(
  gulp.parallel(styles,),
  gulp.series(watchFiles,),
);
